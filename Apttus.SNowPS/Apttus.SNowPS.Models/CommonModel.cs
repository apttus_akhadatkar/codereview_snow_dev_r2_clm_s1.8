﻿/*************************************************************

@Author: Nilesh Keshwala    
@CreateDate: 08-Nov-2017
@Description: 
@UsedBy: Description of how this class is being used
******************************************************************/

namespace Apttus.SNowPS.Model
{

   
    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
