﻿/*************************************************************
@Name: ParticipantModel.cs
@Author: Bhavinkumar Mistry    
@CreateDate: 01-Nov-2017
@Description: This class contains classes & properties for 'Participant' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using System;
using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    /// <summary>
    /// Participant Model
    /// </summary>
    public class ParticipantModel
    {
        /// <summary>
        /// Context Object - Composite
        /// </summary>
        public ContextObject ContextObject { get; set; }

        /// <summary>
        /// Participent - Composite
        /// </summary>
        public ContextObject Participent { get; set; }

        /// <summary>
        /// External Id - String
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// Id - Unique Identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Record Name - String
        /// </summary>
        public string Name { get; set; }
    }
}
