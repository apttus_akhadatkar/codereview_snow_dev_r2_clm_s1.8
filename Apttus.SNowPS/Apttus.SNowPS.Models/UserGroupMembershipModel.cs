﻿/*************************************************************
@Name: UserGroupMembershipModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 21-Nov-2017
@Description: This class contains classes & properties for 'UserGroupMembership' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class UserGroupMembershipModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "UserGroupId")]
        public LookUp UserGroupId { get; set; }

        [JsonProperty(PropertyName = "UserId")]
        public LookUp UserId { get; set; }

        [JsonProperty(PropertyName = "ext_RoleId")]
        public LookUp RoleId { get; set; }

        [JsonProperty(PropertyName = "ext_IsActive")]
        public bool IsActive { get; set; }
    }

}
