﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model
{
    public class AgreementModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "mdmID")]
        public string AccountMDMID { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string RecordTypeId { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "number")]
        public string ContractNumber { get; set; }

        [JsonProperty(PropertyName = "snNumber")]
        public string SNAgreementNumber { get; set; }

        [JsonProperty(PropertyName = "agreementReferenceID")]
        public string CustomerContractNumber { get; set; }

        [JsonProperty(PropertyName = "consumerUniqueNumber")]
        public string AgreementNumber { get; set; }

        [JsonProperty(PropertyName = "quoteID")]
        public string QuoteId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "parentAgreementID")]
        public string ParentAgreementId { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public DateTime? AgreementStartDate { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public DateTime? AgreementEndDate { get; set; }

        [JsonProperty(PropertyName = "term")]
        public Int32? Term { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "statusCategory")]
        public string StatusCategory { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string BaseCurrency { get; set; }

        [JsonProperty(PropertyName = "partnerMDMID")]
        public string partnerAccount { get; set; }

        [JsonProperty(PropertyName = "acv")]
        public decimal? ACV { get; set; }

        [JsonProperty(PropertyName = "acvInUSD")]
        public decimal? USDACV { get; set; }

        [JsonProperty(PropertyName = "tcv")]
        public decimal? TCV { get; set; }

        [JsonProperty(PropertyName = "tcvInUSD")]
        public decimal? USDTCV { get; set; }

        [JsonProperty(PropertyName = "totalAnnualListPrice")]
        public decimal? AnnualListPrice { get; set; }

        [JsonProperty(PropertyName = "totalAnnualListPriceInUSD")]
        public decimal? USDAnnualListPrice { get; set; }

        [JsonProperty(PropertyName = "creationDate")]
        public DateTime? CreatedOn { get; set; }

        [JsonProperty(PropertyName = "createdBy")]
        public string CreatedById { get; set; }

        [JsonProperty(PropertyName = "activationDate")]
        public DateTime? ActivatedDate { get; set; }

        [JsonProperty(PropertyName = "activatedBy")]
        public string ActivatedById { get; set; }

        [JsonProperty(PropertyName = "opportunityID")]
        public string OpportunitySysId { get; set; }

        [JsonProperty(PropertyName = "partnershipType")]
        public string PartnershipType { get; set; }

        [JsonProperty(PropertyName = "territoryMDMID")]
        public string TerritoryMDMID { get; set; }

        //[JsonProperty(PropertyName = "Test")] //ext_RequestorID
        //public string RequestedBy { get; set; } //RequestorID

        [JsonProperty(PropertyName = "quoteOwnerID")]
        public string QuoteOwner { get; set; }

        [JsonProperty(PropertyName = "documents")]
        public List<Document> Documents { get; set; }

    }

	public class Document
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }
		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }
	}

	/// <summary>
	/// Apptus Response Fields
	/// </summary>
	public class AgreementResponse
	{
		public string Id { get; set; }

		[JsonProperty(PropertyName = "ext_AccountMDMID")]
		public string AccountMDMID { get; set; }
		public LookUp RecordTypeId { get; set; }

		[JsonProperty(PropertyName = "ext_Category")]
		public SelectOption Category { get; set; }

		[JsonProperty(PropertyName = "ext_ContractNumber")]
		public string ContractNumber { get; set; }

		[JsonProperty(PropertyName = "ext_CustomerContractNumber")]
		public string CustomerContractReference { get; set; }

		[JsonProperty(PropertyName = "ext_ServiceNowContractNumber")]
		public string SNAgreementNumber { get; set; }

		public string AgreementNumber { get; set; }

		[JsonProperty(PropertyName = "ext_quoteid")]
		public LookUp QuoteId { get; set; }
		public string Name { get; set; }
		public LookUp ParentAgreementId { get; set; }
		public DateTime? AgreementStartDate { get; set; }
		public DateTime? AgreementEndDate { get; set; }

		[JsonProperty(PropertyName = "ext_Term")]
		public Int32? Term { get; set; }
		public SelectOption Status { get; set; }
		public SelectOption StatusCategory{ get; set; }

		[JsonProperty(PropertyName = "ext_BaseCurrency")]
		public ChoiceType BaseCurrency { get; set; }

		[JsonProperty(PropertyName = "ext_PartnerAccountMDMID")]
		public string partnerAccount { get; set; }

		[JsonProperty(PropertyName = "ext_ACV")]
		public decimal? ACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDACV")]
		public decimal? USDACV { get; set; }

		[JsonProperty(PropertyName = "ext_TCV")]
		public decimal? TCV { get; set; }

		[JsonProperty(PropertyName = "ext_USDTCV")]
		public decimal? USDTCV { get; set; }

		[JsonProperty(PropertyName = "ext_AnnualListPrice")]
		public decimal? AnnualListPrice { get; set; }

		[JsonProperty(PropertyName = "ext_USDAnnualListPrice")]
		public decimal? USDAnnualListPrice { get; set; }
		public DateTime? CreatedOn { get; set; }
		public LookUp CreatedById { get; set; }
		public DateTime? ActivatedDate { get; set; }
		public LookUp ActivatedById { get; set; }

		[JsonProperty(PropertyName = "ext_OpportunitySysId")]
		public string OpportunitySysId { get; set; }

		[JsonProperty(PropertyName = "ext_partnershiptype")]
		public SelectOption PartnershipType { get; set; }

		[JsonProperty(PropertyName = "ext_FieldTerritoryMDMID")]
		public string TerritoryMDMID { get; set; }

		//[JsonProperty(PropertyName = "ext_AgreementOriginatorUser")] //ext_RequestorID
		//public LookUp RequestedBy { get; set; } //RequestorID

		[JsonProperty(PropertyName = "ext_QuoteOwner")]
		public string QuoteOwner { get; set; } 

		[JsonProperty(PropertyName = "clm_AgreementDocument")]
		public  AgreementDocument Document { get; set; }

		
	}
	
	public class AgreementDocument
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public SelectOption Type { get; set; }
	}
}
