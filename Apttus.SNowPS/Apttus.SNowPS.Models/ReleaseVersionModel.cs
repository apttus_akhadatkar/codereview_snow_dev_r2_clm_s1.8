﻿/****************************************************************************************
@Name: ReleaseVersionModel.cs
@Author: Bharat Kumbhar
@CreateDate: 24 Oct 2017
@Description: Model class for the ReleaseVersion object.
@UsedBy: 

@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
*****************************************************************************************/

using Newtonsoft.Json;
using System;

namespace Apttus.SNowPS.Model
{
    /// <summary>
    /// Model class for the ReleaseVersion object.
    /// </summary>
    public class ReleaseVersionModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ReleaseVersionModel"/> is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_Active")]
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [early release default].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [early release default]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_EarlyReleaseDefault")]
        public bool EarlyReleaseDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enterprise internal].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enterprise internal]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_EnterpriseInternal")]
        public bool EnterpriseInternal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enterprise new instance default].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enterprise new instance default]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_EnterpriseNewInstanceDefault")]
        public bool EnterpriseNewInstanceDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [express internal].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [express internal]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_ExpressInternal")]
        public bool ExpressInternal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [express new instance default].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [express new instance default]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_ExpressNewInstanceDefault")]
        public bool ExpressNewInstanceDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fedramp new instance default].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fedramp new instance default]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_FedrampNewInstanceDefault")]
        public bool FedrampNewInstanceDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [oem new instance default].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [oem new instance default]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_OEMNewInstanceDefault")]
        public bool OEMNewInstanceDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ReleaseVersionModel"/> is public.
        /// </summary>
        /// <value>
        ///   <c>true</c> if public; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("ext_Public")]
        public bool Public { get; set; }

        /// <summary>
        /// Gets or sets the release.
        /// </summary>
        /// <value>
        /// The release.
        /// </value>
        [JsonProperty("ext_Release")]
        public string Release { get; set; }

        /// <summary>
        /// Gets or sets the release availability.
        /// </summary>
        /// <value>
        /// The release availability.
        /// </value>
        [JsonProperty("ext_ReleaseAvailability")]
        public string ReleaseAvailability { get; set; }

        /// <summary>
        /// Gets or sets the release version.
        /// </summary>
        /// <value>
        /// The release version.
        /// </value>
        [JsonProperty("ext_ReleaseVersion")]
        public string ReleaseVersion { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
    }
}
