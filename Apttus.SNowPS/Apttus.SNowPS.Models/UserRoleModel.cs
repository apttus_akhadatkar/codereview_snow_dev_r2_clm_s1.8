﻿/*************************************************************
@Name: UserRoleModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 08-Nov-2017
@Description: This class contains classes & properties for 'Prodcut group' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class UserRoleModel
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "cmn_RoleId")]
        public LookUp RoleId { get; set; }

        [JsonProperty(PropertyName = "crm_UserId")]
        public LookUp UserId { get; set; }

        [JsonProperty(PropertyName = "ext_IsActive")]
        public bool IsActive { get; set; }
    }

}
