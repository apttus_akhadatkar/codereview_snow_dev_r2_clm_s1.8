﻿/****************************************************************************************
@Name: Quote.cs
@Author: Kiran Satani
@CreateDate: 13 Oct 2017
@Description: All require attributes of Apttus AssetLineItem  for mule AssetLineItem sync 
@UsedBy: This will be used by API to get AssetLineItem Details

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class AssetLineItemModel
    {
        public string id { get; set; }
        public string accountName { get; set; }
        public string status { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public Int32? parentBundleNumber { get; set; }
        public string productID { get; set; }
        public string agreementID { get; set; }
        public string productName { get; set; }
        public string optionId { get; set; }
        public string optionName { get; set; }
        public Decimal? netPrice { get; set; }
        //public DateTime? CreatedOn { get; set; }
        public Int32? itemSequence { get; set; }
        public Int32? lineNumber { get; set; }
        public Int32? primaryLineNumber { get; set; }
        public Decimal? quantity { get; set; }
        public string dataCenter { get; set; }
        public string physicalDataCenter { get; set; }
        public string parentAgreementNumber { get; set; }
        public Int32? parentassetLineNumber { get; set; }
        public int? assetLineNumber { get; set; }
        public string releaseVersion { get; set; }
        public string hiChgRequestNumber { get; set; }
        public string instanceId { get; set; }
        public string instanceName { get; set; }
        public string instanceMappingID { get; set; }
        public string instanceMappingName { get; set; }
        public Decimal? annualContractValue { get; set; }
        public Decimal? annualContractValueInUSD { get; set; }
        public Decimal? annualListPrice { get; set; }
        public Decimal? annualListPriceInUSD { get; set; }
        public string businessUnit { get; set; }
        public Decimal? adjustmentInUSD { get; set; }
        public Decimal? basePriceInUSD { get; set; }
        public Decimal? listPriceInUSD { get; set; }
        public Decimal? totalValueInUSD { get; set; }
        public Decimal? salesPriceInUSD { get; set; }
        public Decimal? allocationPercentage { get; set; }
        public string deploymentNumber { get; set; }
        public string icLevel { get; set; }
        public string practice { get; set; }
        public string resourcePlan { get; set; }
        public bool? areResourceRolesFixedFeeItems { get; set; }
        public string soldMappingID { get; set; }
        public string soldMappingName { get; set; }
        public string taskID { get; set; }
        public string contractNumber { get; set; }
        public string operationalStatus { get; set; }
        public DateTime? instanceCreationDate { get; set; }
        public Decimal? adjustedPrice { get; set; }
        public string actionsAllowed { get; set; }
        public Decimal? assetARR { get; set; }
        public string assetcode { get; set; }
        public Decimal? assetMRR { get; set; }
        public string assetNumber { get; set; }
        public Decimal? tcv { get; set; }
        public string attributeValueID { get; set; }
        public bool isAutoRenew { get; set; }
        public string autoRenewalType { get; set; }
        public Decimal? availableBalance { get; set; }
        public Decimal? baseCost { get; set; }
        public Decimal? baseCostExtended { get; set; }
        public Decimal? basePriceExtended { get; set; }
        public string basePriceMethod { get; set; }
        public Decimal? basePrice { get; set; }
        public DateTime? billThroughDate { get; set; }
        public string billToMDMID { get; set; }
        public DateTime? billingEndDate { get; set; }
        public string billingFrequency { get; set; }
        public string billingPlanID { get; set; }
        public string billingPreferenceID { get; set; }
        public string billingRule { get; set; }
        public DateTime? billingStartDate { get; set; }
        public string bundleLineItemID { get; set; }
        public string busineesLineItemID { get; set; }
        public string businessObjectID { get; set; }
        public string busineesObjectType { get; set; }
        public DateTime? cancellationDate { get; set; }
        public string chargeType { get; set; }
        public string comments { get; set; }
        public Decimal? deltaPrice { get; set; }
        public Decimal? deltaQuantity { get; set; }
        public string description { get; set; }
        public Decimal? extendedCost { get; set; }
        public string extendedDescription { get; set; }
        public Decimal? extendedPrice { get; set; }
        public string pricingfrequencyType { get; set; }
        public bool? hasAttributes { get; set; }
        public bool? hasOptions { get; set; }
        public bool? hideInvoiceDisplay { get; set; }
        public DateTime? initialActivationDate { get; set; }
        public bool? isOptionRoleUpLine { get; set; }
        public bool? isPrimaryLine { get; set; }
        public bool? isPrimaryRampLine { get; set; }
        public bool? isReadOnly { get; set; }
        public bool? isRenewalPending { get; set; }
        public bool? isRenewed { get; set; }
        public bool? isUsageTierModifiable { get; set; }
        public DateTime? lastRenewEndDate { get; set; }
        public string lineType { get; set; }
        public Decimal? listPrice { get; set; }
        public string locationID { get; set; }
        public Decimal? maxUsageQuantity { get; set; }
        public Decimal? minUsageQuantity { get; set; }
        public bool? mustUpgrade { get; set; }
        public string name { get; set; }
        public Decimal? netUnitPrice { get; set; }
        public DateTime? nextRenewalEndDate { get; set; }
        public Decimal? optionCost { get; set; }
        public Decimal? optionPrice { get; set; }
        public DateTime? originalStartDate { get; set; }
        public string parentAssetID { get; set; }
        public string paymentTermID { get; set; }
        public string priceGroup { get; set; }
        public bool? isPriceIncluded { get; set; }
        public string priceListID { get; set; }
        public string priceListItemID { get; set; }
        public string priceMethodType { get; set; }
        public string priceType { get; set; }
        public string priceUOM { get; set; }
        public DateTime? pricingDate { get; set; }
        public string productType { get; set; }
        public DateTime? purchaseDate { get; set; }
        public Decimal? renewalAdjustmentAmount { get; set; }
        public string renewalAdjustmentType { get; set; }
        public DateTime? renewalDate { get; set; }
        public string renewalFrequencyType { get; set; }
        public Decimal? renewalTerm { get; set; }
        public string sellingFrequencyType { get; set; }
        public Decimal? sellingTerm { get; set; }
        public string shipToMDMID { get; set; }
        public string taxCodeID { get; set; }
        public bool? isTaxInclusive { get; set; }
        public bool? isTaxable { get; set; }
        public Int32? term { get; set; }
        public Decimal? totalBalance { get; set; }
        public string quoteID { get; set; }
        public string quoteLineItemID { get; set; }
    }

    /***********************  Response Quote from Apptus *******************************/

    public class ResponseAssetLineItem
    {
        public string Id { get; set; }
        public LookUp AccountId { get; set; }
        public SelectOption AssetStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Int32? ParentBundleNumber { get; set; }
        public LookUp ProductId { get; set; }
        public LookUp ext_AgreementId { get; set; }
        public LookUp OptionId { get; set; }
        public Decimal? NetPrice { get; set; }
        public DateTime? ext_InstanceCreatedOn { get; set; }
        public Int32? ItemSequence { get; set; }
        public Int32? LineNumber { get; set; }
        public Int32? PrimaryLineNumber { get; set; }
        public Decimal? Quantity { get; set; }
        public string Name { get; set; }
        public Decimal? AdjustedPrice { get; set; }
        public string[] AllowedActions { get; set; }
        public Decimal? AssetARR { get; set; }
        public string AssetCode { get; set; }
        public Decimal? AssetMRR { get; set; }
        public string AssetNumber { get; set; }
        public Decimal? AssetTCV { get; set; }
        public LookUp AttributeValueId { get; set; }
        public bool AutoRenew { get; set; }
        public SelectOption AutoRenewalType { get; set; }
        public Decimal? AvailableBalance { get; set; }
        public Decimal? BaseCost { get; set; }
        public Decimal? BaseExtendedCost { get; set; }
        public Decimal BaseExtendedPrice { get; set; }
        public Decimal BasePrice { get; set; }
        public SelectOption BasePriceMethod { get; set; }
        public DateTime? BillingEndDate { get; set; }
        public DateTime? BillThroughDate { get; set; }
        public LookUp BillToAccountId { get; set; }
        public SelectOption BillingFrequency { get; set; }
        public LookUp BillingPlanId { get; set; }
        public LookUp BillingPreferenceId { get; set; }
        public SelectOption BillingRule { get; set; }
        public DateTime? BillingStartDate { get; set; }
        public LookUp BundleAssetId { get; set; }
        public string BusinessLineItemId { get; set; }
        public string BusinessObjectId { get; set; }
        public SelectOption BusinessObjectType { get; set; }
        public DateTime? CancelledDate { get; set; }
        public SelectOption ChargeType { get; set; }
        public string Comments { get; set; }
        public string ext_Datacenter { get; set; }
        public string ext_ReleaseVersion { get; set; }
        public string ext_DCInstanceID { get; set; }
        public string ext_InstanceName { get; set; }
        public string ext_OperationStatus { get; set; }
        public string ext_ChangeTicket { get; set; }
        public string ext_billToMDMID { get; set; }
        public string ext_shipToMDMID { get; set; }
        public string ext_AccountMDMId { get; set; }
        public string ext_ServiceNowContractNumber { get; set; }
        public string ext_PhysicalDataCentre { get; set; }
        public string ext_InstalledMappingID { get; set; }
        public string ext_InstalledMappingName { get; set; }
        public Decimal? ext_AnnualContractValue { get; set; }
        public Decimal? ext_USDAnnualContractValue { get; set; }
        public Decimal? ext_AnnualListPrice { get; set; }
        public Decimal? ext_USDAnnualListPrice { get; set; }
        public string ext_BusinessUnit { get; set; }
        public Decimal? ext_USDAdjustment { get; set; }
        public Decimal? ext_USDBasePrice { get; set; }
        public Decimal? ext_USDListPrice { get; set; }
        public Decimal? ext_USDTotalValue { get; set; }
        public Decimal? ext_USDSalesPrice { get; set; }
        public Decimal? ext_AllocationPercent { get; set; }
        public string ext_Deployment { get; set; }
        public SelectOption ext_ICLevel { get; set; }
        public SelectOption ext_Practice { get; set; }
        public string ext_ResourcePlan { get; set; }
        public bool? ext_ResourceRolesFixedFeeItems { get; set; }
        public string ext_SoldMappingID { get; set; }
        public string ext_SoldMappingName { get; set; }
        public string ext_TaskID { get; set; }
        public Decimal? DeltaPrice { get; set; }
        public Decimal? DeltaQuantity { get; set; }
        public string Description { get; set; }
        public Decimal? ExtendedCost { get; set; }
        public string ExtendedDescription { get; set; }
        public Decimal? ExtendedPrice { get; set; }
        public SelectOption Frequency { get; set; }
        public bool? HasAttributes { get; set; }
        public bool? HasOptions { get; set; }
        public bool? HideInvoiceDisplay { get; set; }
        public DateTime? InitialActivationDate { get; set; }
        public bool? IsInactive { get; set; }
        public bool? IsOptionRollupLine { get; set; }
        public bool? IsPrimaryLine { get; set; }
        public bool? IsPrimaryRampLine { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsRenewalPending { get; set; }
        public bool? IsRenewed { get; set; }
        public bool? IsUsageTierModifiable { get; set; }
        public DateTime? LastRenewEndDate { get; set; }
        public SelectOption LineType { get; set; }
        public Decimal? ListPrice { get; set; }
        public LookUp LocationId { get; set; }
        public Decimal? MaxUsageQuantity { get; set; }
        public Decimal? MinUsageQuantity { get; set; }
        public bool? MustUpgrade { get; set; }
        public Decimal? NetUnitPrice { get; set; }
        public DateTime? NextRenewEndDate { get; set; }
        public Decimal? OptionCost { get; set; }
        public Decimal? OptionPrice { get; set; }
        public DateTime? OriginalStartDate { get; set; }
        public LookUp ParentAssetId { get; set; }
        public LookUp PaymentTermId { get; set; }
        public SelectOption PriceGroup { get; set; }
        public bool? PriceIncludedInBundle { get; set; }
        public LookUp PriceListId { get; set; }
        public LookUp PriceListItemId { get; set; }
        public SelectOption PriceMethod { get; set; }
        public SelectOption PriceType { get; set; }
        public SelectOption PriceUom { get; set; }
        public DateTime? PricingDate { get; set; }
        public SelectOption ProductType { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public Decimal? RenewalAdjustmentAmount { get; set; }
        public SelectOption RenewalAdjustmentType { get; set; }
        public DateTime? RenewalDate { get; set; }
        public SelectOption RenewalFrequency { get; set; }
        public Decimal? RenewalTerm { get; set; }
        public SelectOption SellingFrequency { get; set; }
        public Decimal? SellingTerm { get; set; }
        public LookUp ShipToAccountId { get; set; }
        public LookUp TaxCodeId { get; set; }
        public bool? TaxInclusive { get; set; }
        public bool? Taxable { get; set; }
        public Int32? Term { get; set; }
        public Decimal? TotalBalance { get; set; }
        public LookUp QuoteId { get; set; }
        public LookUp QuoteLineItemId { get; set; }

    }

    public class AssetMuleRequest
    {
        public string accountName { get; set; }
        public string mdmID { get; set; }
        public List<AssetLineItemModel> lineItems { get; set; }
    }
}
