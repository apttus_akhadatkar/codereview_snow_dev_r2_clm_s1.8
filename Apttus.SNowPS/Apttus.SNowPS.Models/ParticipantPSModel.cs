﻿/*************************************************************
@Name: ParticipantModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 06-Nov-2017
@Description: This class contains classes & properties for 'Participant' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class ParticipantPSModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ContextObj ContextObject { get; set; }
        public Participant Participant { get; set; }

        [JsonProperty(PropertyName = "cmn_ParticipantRole")]
        public LookUp Role { get; set; }
    }

    public class ContextObj
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }
    public class Participant
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }
}
