﻿/****************************************************************************************
@Name: HttpActions.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Defines Http Action Methods GET, POST, UPSERT 
@UsedBy: This will be used to call Get/Post/Upsert Methods

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Model;
using System;
using System.Net;
using System.Net.Http;

namespace Apttus.SNowPS.Common
{
    public static class HttpActions
    {
        /// <summary>
        /// Post Request to Generic API
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="url"></param>
        /// <param name="httpContent"></param>
        /// <returns></returns>
        public static HttpResponseMessage PostRequest(string accessToken, string url, StringContent httpContent, string appUrl)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                client.DefaultRequestHeaders.Add("Authorization", accessToken);
                if(!string.IsNullOrEmpty(appUrl))
                    client.DefaultRequestHeaders.Add("App-Url", appUrl);

                var responseMessge = client.PostAsync(url, httpContent).Result;

                return responseMessge;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Put Request to Generic API
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="url"></param>
        /// <param name="httpContent"></param>
        /// <returns></returns>
        public static HttpResponseMessage PutRequest(string accessToken, string url, StringContent httpContent)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                client.DefaultRequestHeaders.Add("Authorization", accessToken);
                var responseMessge = client.PutAsync(url, httpContent).Result;                
                return responseMessge;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Put Request to Mule API
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="url"></param>
        /// <param name="xClientId"></param>
        /// <param name="xSourceSystem"></param>
        /// <param name="httpContent"></param>
        /// <returns></returns>
        public static HttpResponseMessage PutRequestMule(MuleHeaderModel muleHeaderModel)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.DefaultRequestHeaders.Add("Authorization", muleHeaderModel.AccessToken);
                client.DefaultRequestHeaders.Add("X-Client-ID", muleHeaderModel.XClientId);
                client.DefaultRequestHeaders.Add("X-SourceSystem", muleHeaderModel.XSourceSystem);
                client.DefaultRequestHeaders.Add("x-batchid", muleHeaderModel.XBatchId);
                muleHeaderModel.HttpContentStr.Headers.ContentType.MediaType = "application/json";
                var responseMessge = client.PutAsync(string.Format(muleHeaderModel.MuleUrl, muleHeaderModel.APIName), muleHeaderModel.HttpContentStr).Result;

                return responseMessge;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Get Request to Generic API
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="url"></param>
        /// <param name="appUrl"></param>
        /// <returns></returns>
        public static HttpResponseMessage GetRequest(string accessToken, string url, string appUrl)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                client.DefaultRequestHeaders.Add("Authorization", accessToken);
                if(!string.IsNullOrEmpty(appUrl))
                    client.DefaultRequestHeaders.Add("App-Url", appUrl);

                var responseMessge = client.GetAsync(url).Result;

                if (responseMessge.IsSuccessStatusCode)
                    responseString = responseMessge.Content.ReadAsStringAsync().Result;
                else
                    responseString = responseMessge.ReasonPhrase;

                return responseMessge;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Get Request to Mule API
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="url"></param>
        /// <param name="appUrl"></param>
        /// <param name="X-Client-ID"></param>
        /// <returns></returns>
        public static HttpResponseMessage GetRequestMule(MuleHeaderModel muleHeaderModel)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.DefaultRequestHeaders.Add("Authorization", muleHeaderModel.AccessToken);
                client.DefaultRequestHeaders.Add("App-Url", muleHeaderModel.AppUrl);
                client.DefaultRequestHeaders.Add("X-Client-ID", muleHeaderModel.XClientId);
                var responseMessge = client.GetAsync(string.Format(muleHeaderModel.MuleUrl, muleHeaderModel.APIName)).Result;
                return responseMessge;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Post Request to Mule
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="xClientId"></param>
        /// <param name="url"></param>
        /// <param name="httpContent"></param>
        /// <param name="xSourceSystem"></param>
        /// <param name="sendType"></param>
        /// <returns></returns>
        public static HttpResponseMessage PostRequestMule(MuleHeaderModel muleHeaderModel)
        {
            var client = new HttpClient();
            var responseString = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.DefaultRequestHeaders.Add("X-Client-ID", muleHeaderModel.XClientId);
				if (!string.IsNullOrEmpty(muleHeaderModel.AccessToken))
					client.DefaultRequestHeaders.Add("Authorization", muleHeaderModel.AccessToken);
				if (!string.IsNullOrEmpty(muleHeaderModel.XSourceSystem))
					client.DefaultRequestHeaders.Add("X-SourceSystem", muleHeaderModel.XSourceSystem);
				if (!string.IsNullOrEmpty(muleHeaderModel.TargetSystem))
					client.DefaultRequestHeaders.Add("targetSystem", muleHeaderModel.TargetSystem);
                if (!string.IsNullOrEmpty(muleHeaderModel.XBatchId))
                    client.DefaultRequestHeaders.Add("X-BatchId", muleHeaderModel.XBatchId);

                var responseMessge = client.PostAsync(string.Format(muleHeaderModel.MuleUrl,muleHeaderModel.APIName), muleHeaderModel.HttpContentStr).Result;
                return responseMessge;
            }
            catch
            {
                throw;
            }
        }

    }
}
