﻿/****************************************************************************************
@Name: Utilities.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Defines Common Methods 
@UsedBy: This will be used by all other classes as a utility

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Common
{
    public static class Utilities
    {
        static string appInstance = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPINSTANCE]);

        /// <summary>
        /// Get Auth Token
        /// </summary>
        /// <returns></returns>
        public static string GetAuthToken()
        {
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(Constants.CACHE_ACCESSTOKEN))
            {
                return cache.Get(Constants.CACHE_ACCESSTOKEN).ToString();
            }
            else
            {
                //var newAccessToken = GetAuthToken();
                string authToken = string.Empty;
                var url = Convert.ToString(ConfigurationManager.AppSettings["AuthUrl"]);
                var clientID = Convert.ToString(ConfigurationManager.AppSettings["ClientId"]);
                var clientSecret = Convert.ToString(ConfigurationManager.AppSettings["ClientSecret"]);

                var formContent = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", clientID),
                    new KeyValuePair<string, string>("client_secret", clientSecret),
                    new KeyValuePair<string, string>("resource", clientID)
                });

                HttpClient httpClient = new HttpClient();

                HttpResponseMessage messge = httpClient.PostAsync(url, formContent).Result;
                if (messge.IsSuccessStatusCode)
                {
                    var resString = messge.Content.ReadAsStringAsync().Result;

                    var accessToken = JsonConvert.DeserializeObject<AccessTokenModel>(resString);

                    if (accessToken != null)
                        authToken = accessToken.TokenType + " " + accessToken.Token;
                }

                // Store data in the cache    
                cache.Add(Constants.CACHE_ACCESSTOKEN, authToken, new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddSeconds(3400) });

                return authToken;
            }
        }

        /// <summary>
        /// Create serach filters
        /// </summary>
        /// <param name="requestData"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage Search(List<Dictionary<string, object>> requestData, RequestConfigModel reqConfig)
        {
            RequestModel requestModel = new RequestModel();
            requestModel.logicalExpression = null;
            requestModel.select = reqConfig.select;
            requestModel.filter = new List<FilterModel>();

            var filterValue = requestData.Where(data => data.ContainsKey(reqConfig.externalFilterField) && !string.IsNullOrEmpty(Convert.ToString(data[reqConfig.externalFilterField]))).Select(data => Convert.ToString(data[reqConfig.externalFilterField])).Distinct().ToArray();

            requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = "In", value = filterValue });

            var jsonQuery = JsonConvert.SerializeObject(requestModel);

            var responseString = Search(jsonQuery, reqConfig);

            return responseString;
        }

        /// <summary>
        /// Search based on GET
        /// </summary>
        /// <param name="jsonQuery"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage GetSearch(string jsonQuery, RequestConfigModel reqConfig)
        {
            var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_GETSEARCHAPI]);
            var appUrl = Convert.ToString(ConfigurationManager.AppSettings["AppUrl"]);

            var url = appInstance + string.Format(searchAPI, reqConfig.objectName, jsonQuery);

            var responseString = HttpActions.GetRequest(reqConfig.accessToken, url, appUrl);

            return responseString;
        }

        /// <summary>
        /// Get Metadata
        /// </summary>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage GetMetadata(RequestConfigModel reqConfig)
        {
            var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_METADATAAPI]);

            var url = appInstance + string.Format(searchAPI, reqConfig.objectName);

            var responseString = HttpActions.GetRequest(reqConfig.accessToken, url, null);

            return responseString;
        }

        /// <summary>
        /// Search based on POST
        /// </summary>
        /// <param name="jsonQuery"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage Search(string jsonQuery, RequestConfigModel reqConfig)
        {
            string searchAPIType = Constants.CONFIG_SEARCHAPI;
            if (reqConfig.searchType == SearchType.AQL)
                searchAPIType = Constants.CONFIG_AQLSEARCHAPI;
            else if (reqConfig.searchType == SearchType.CpqAdminAPI)
                searchAPIType = Constants.CONFIG_CPQADMINAPI;

            var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[searchAPIType]);
            var url = appInstance + string.Format(searchAPI, reqConfig.objectName);

            var httpContent = new StringContent(jsonQuery, Encoding.UTF8, "application/json");

            var responseString = HttpActions.PostRequest(reqConfig.accessToken, url, httpContent, reqConfig.appUrl);

            return responseString;
        }

        /// <summary>
        /// Create Records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestData"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage Create<T>(List<T> requestData, RequestConfigModel reqConfig)
        {
            var CreatedID = string.Empty;
            var createAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_CREATEAPI]);
            var url = string.Format(appInstance + createAPI, reqConfig.objectName);

            //Create Query
            string jsonString = JsonConvert.SerializeObject(requestData);

            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var responseString = HttpActions.PostRequest(reqConfig.accessToken, url, httpContent, null);

            return responseString;
        }

        /// <summary>
        /// Update Records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestData"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage Update<T>(List<T> requestData, RequestConfigModel reqConfig)
        {
            var CreatedID = string.Empty;
            var createAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_CREATEAPI]);
            var url = string.Format(appInstance + createAPI, reqConfig.objectName);

            //Create Query
            string jsonString = JsonConvert.SerializeObject(requestData);

            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = HttpActions.PutRequest(reqConfig.accessToken, url, httpContent);

            return response;
        }

        /// <summary>
        /// Upsert Records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestData"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage Upsert<T>(List<T> requestData, RequestConfigModel reqConfig, bool hasUpsertKey = true)
        {
            var upsertAPI = String.Empty;
            var url = String.Empty;
            if (hasUpsertKey)
            {
                upsertAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_UPSERTAPI]);
                url = string.Format(appInstance + upsertAPI, reqConfig.objectName, reqConfig.UpsertKey);
            }
            else
            {
                upsertAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_UPSERTAPIWOKey]);
                url = string.Format(appInstance + upsertAPI, reqConfig.objectName);
            }

            //Create Query
            string jsonString = JsonConvert.SerializeObject(requestData);

            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var responseString = HttpActions.PostRequest(reqConfig.accessToken, url, httpContent, null);

            return responseString;
        }

        /// <summary>
        /// Delete Multiple Records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestData"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static HttpResponseMessage DeleteMultiple(List<Dictionary<string, object>> requestData, RequestConfigModel reqConfig)
        {
            var deleteAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_DELETEMULTIPLEAPI]);
            var url = string.Format(appInstance + deleteAPI, reqConfig.objectName);

            //Create Query
            var IDs = requestData.Select(data => data.ContainsKey(Constants.FIELD_ID) && !string.IsNullOrEmpty(Convert.ToString(Constants.FIELD_ID)) ? Convert.ToString(data[Constants.FIELD_ID]) : null).Distinct().Where(data => data != null).ToArray();
            string jsonString = JsonConvert.SerializeObject(IDs);

            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = HttpActions.PostRequest(reqConfig.accessToken, url, httpContent, null);

            return response;
        }

        /// <summary>
        /// Resolve External IDs
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public static void ResolveExternalID(List<Dictionary<string, object>> dictContent, RequestConfigModel reqConfig, List<ErrorInfo> errors)
        {
            //Resolve Account MDM Id with Apttus Account Id
            var responseMessage = Search(dictContent, reqConfig);
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;

            if (responseMessage != null && responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {
                var response = JObject.Parse(responseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>();
                response = GetCaseIgnoreDictContent(response);

                //Get the ExternalIDs which are not resolved.
                var requestExternalIDs = new List<Dictionary<string, object>>();
                Parallel.ForEach(dictContent, content => {
                    if (content.ContainsKey(reqConfig.externalFilterField) && content.ContainsKey(reqConfig.apttusFilterField) && !string.IsNullOrEmpty(Convert.ToString(content[reqConfig.externalFilterField])))
                    {
                        var requestExternalID = new Dictionary<string, object>();
                        requestExternalID.Add(reqConfig.apttusFilterField, content[reqConfig.apttusFilterField]);
                        if(!requestExternalID.ContainsKey(reqConfig.externalFilterField))
                            requestExternalID.Add(reqConfig.externalFilterField, content[reqConfig.externalFilterField]);
                        requestExternalIDs.Add(requestExternalID);
                    }
                });

                //var requestExternalIDs = (from Dictionary<string, object> content in dictContent where content.ContainsKey(reqConfig.externalFilterField) && content.ContainsKey(reqConfig.apttusFilterField) && !string.IsNullOrEmpty(Convert.ToString(content[reqConfig.externalFilterField])) select new Dictionary<string, object>() { { reqConfig.apttusFilterField, content[reqConfig.apttusFilterField] }, { reqConfig.externalFilterField, content[reqConfig.externalFilterField] } }).ToList();
                var responseExternalIDs = (from Dictionary<string, object> content in response where content.ContainsKey(reqConfig.apttusFilterField) select new Dictionary<string, object>() { { reqConfig.externalFilterField, content[reqConfig.apttusFilterField] } }).ToList();

                var notAvailableIDs = requestExternalIDs.Where(request => !responseExternalIDs.Exists(res => Convert.ToString(res[reqConfig.externalFilterField]) == Convert.ToString(request[reqConfig.externalFilterField])));

                if (notAvailableIDs != null && notAvailableIDs.Count() > 0)
                {
                    foreach (Dictionary<string, object> notAvailableID in notAvailableIDs)
                    {
                        if (errors.Count == 0 || !errors.Exists(error => error.Record.ContainsKey(reqConfig.apttusFilterField) && Convert.ToString(error.Record[reqConfig.apttusFilterField]) == Convert.ToString(notAvailableID[reqConfig.apttusFilterField])))
                            errors.Add(new ErrorInfo() { Message = Constants.ERR_UNRESOLVED_MESSAGE, Record = notAvailableID });
                        else
                            errors.Where(error => Convert.ToString(error.Record[reqConfig.apttusFilterField]) == Convert.ToString(notAvailableID[reqConfig.apttusFilterField])).FirstOrDefault().Record.Add(reqConfig.externalFilterField, notAvailableID[reqConfig.externalFilterField]);
                    }
                }

                //Assign id to resolved id
                foreach (var content in dictContent)
                {
                    if (content.ContainsKey(reqConfig.externalFilterField))
                    {
                        //Replace blank external IDs with null
                        if (content[reqConfig.externalFilterField] != null && Convert.ToString(content[reqConfig.externalFilterField]) == string.Empty)
                        {
                            content[reqConfig.externalFilterField] = null;
                        }

                        var resolvedField = response.FirstOrDefault(res => Convert.ToString(res[reqConfig.apttusFilterField]) == Convert.ToString(content[reqConfig.externalFilterField]));
                        if (resolvedField != null)
                            content[reqConfig.resolvedID] = resolvedField["Id"];
                    }
                }

            }
        }

        /// <summary>
        /// Get case insensitive Dictionary object for List of Dictionary 
        /// </summary>
        /// <param name="requestdictContent"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> GetCaseIgnoreDictContent(List<Dictionary<string, object>> requestdictContent)
        {
            List<Dictionary<string, object>> dictContent = new List<Dictionary<string, object>>();
            dictContent.AddRange(requestdictContent.Select(x => x.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase)));
            return dictContent;
        }

        /// <summary>
        /// Get case insensitive Dictionary object for Dictionary 
        /// </summary>
        /// <param name="requestdictContent"></param>
        /// <returns></returns>
        public static Dictionary<string, object> GetCaseIgnoreSingleDictContent(Dictionary<string, object> requestdictContent)
        {
            Dictionary<string, object> dictContent = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            foreach (KeyValuePair<string, object> item in requestdictContent)
            {
                dictContent.Add(item.Key, item.Value);
            }
            return dictContent;
        }

        /// <summary>
        /// Create the response
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static HttpResponseMessage CreateResponse(HttpResponseMessage response)
        {
            var responseString = response.Content.ReadAsStringAsync().Result;

            var responseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseString);

            HttpResponseMessage reponseMessage = new HttpResponseMessage();
            reponseMessage.StatusCode = response.StatusCode;
            reponseMessage.Content = new StringContent(responseString, Encoding.UTF8, "application/json");

            return reponseMessage;
        }

        /// <summary>
        /// Modify response by adding custom validations
        /// </summary>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public static HttpResponseMessage CreateResponse(HttpResponseMessage response, List<ErrorInfo> errors)
        {
            if (response.IsSuccessStatusCode)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;

                var responseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseString);
                var errorJson = JsonConvert.SerializeObject(errors);

                if (errors != null && errors.Count > 0)
                {
                    if (responseObj.Errors == null) responseObj.Errors = new List<ErrorInfo>();
                    responseObj.Errors.AddRange(errors);
                }

                if (responseObj != null && (responseObj.Inserted != null || responseObj.Updated != null || responseObj.Deleted != null || responseObj.Errors != null))
                    responseString = JsonConvert.SerializeObject(responseObj);

                HttpResponseMessage reponseMessage = new HttpResponseMessage();
                reponseMessage.StatusCode = response.StatusCode;
                reponseMessage.Content = new StringContent(responseString, Encoding.UTF8, "application/json");

                return reponseMessage;
            }

            return response;
        }

        /// <summary>
        /// Decode the Querystring
        /// </summary>
        /// <param name="encodedQuery"></param>
        /// <returns></returns>
        public static string GetDecodedQuery(string encodedQuery)
        {
            var decodedQuery = HttpUtility.UrlDecode(encodedQuery);

            decodedQuery = decodedQuery.Replace("?query=", "");

            return decodedQuery;
        }

        /// <summary>
        /// Check if user is existing or new
        /// </summary>
        /// <param name="user"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static bool IsNewUser(Dictionary<string, object> user, string accessToken)
        {
            var config = new RequestConfigModel()
            {
                accessToken = accessToken,
                select = new string[] { Constants.FIELD_EXTERNALID },
                objectName = Constants.OBJ_USER,
                externalFilterField = Constants.FIELD_EXTERNALID,
                apttusFilterField = Constants.FIELD_EXTERNALID,
                resolvedID = Constants.FIELD_EXTERNALID
            };

            List<Dictionary<string, object>> searchUser = new List<Dictionary<string, object>>();
            searchUser.Add(user);
            var searchResponse = Search(searchUser, config);
            var searchResponseString = searchResponse.Content.ReadAsStringAsync().Result;
            var searchResult = JObject.Parse(searchResponseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>();

            if (searchResult != null && searchResult.Count > 0)
            {
                if (searchResult[0].ContainsKey(config.apttusFilterField) && !string.IsNullOrEmpty(Convert.ToString(searchResult[0][config.apttusFilterField])))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Get Mule AccessToken from username and password.
        /// </summary>
        /// <returns></returns>
        public static string GetMuleAuthToken()
        {
            string muleBasicAuthToken = string.Empty;
            var clientID = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_MULEUSERNAME]);
            var clientSecret = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_MULEPASSWORD]);
            muleBasicAuthToken = "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(clientID + ":" + clientSecret));
            return muleBasicAuthToken;
        }

        /// <summary>
        /// Update ExternalID Received in Mule Response
        /// </summary>
        /// <param name="id"></param>
        /// <param name="externalId"></param>
        /// <param name="objectName"></param>
        /// <returns></returns>
        public static HttpResponseMessage UpdateMuleResponse(string id, string externalId, string objectName = null)
        {
            List<Dictionary<string, object>> dictContent = new List<Dictionary<string, object>>();
            RequestConfigModel reqConfig = new RequestConfigModel();

            dictContent.Add(new Dictionary<string, object> { { Constants.FIELD_ID, id }, { Constants.FIELD_EXTERNALID, externalId } });

            reqConfig.accessToken = GetAuthToken();
            reqConfig.objectName = objectName;
            reqConfig.UpsertKey = Constants.FIELD_ID;

            var response = Upsert(dictContent, reqConfig);

            return response;
        }
        /// <summary>
        /// Update ExternalID Received in Mule Response
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="objectName"></param>
        /// <returns></returns>
        public static HttpResponseMessage UpdateMuleResponse(List<Dictionary<string, object>> dictContent, string objectName = null)
        {
            RequestConfigModel reqConfig = new RequestConfigModel();
            reqConfig.accessToken = GetAuthToken();
            reqConfig.objectName = objectName;
            reqConfig.UpsertKey = Constants.FIELD_ID;
            var response = Upsert(dictContent, reqConfig);

            return response;
        }

        /// <summary>
        /// Method use for Get select field of Object.
        /// </summary>
        /// <param name="objectName"></param>
        /// <returns>String[] which contain select field</returns>
        public static string[] GetSelectList(string objectName)
        {
            string[] arrSelectColumn = new string[] { };
            switch (objectName)
            {
                case Constants.OBJ_CONTACT:
                    arrSelectColumn = Constants.OBJ_CONTACT_SELECTFIELD.Split(Constants.CHAR_COMMA);
                    break;
                case Constants.OBJ_ACCOUNT:
                    arrSelectColumn = Constants.OBJ_ACCOUNT_SELECTFIELD.Split(Constants.CHAR_COMMA);
                    break;
                case Constants.OBJ_ACCOUNTLOCATION:
                    arrSelectColumn = Constants.OBJ_ACCOUNTLOCATION_SELECTFIELD.Split(Constants.CHAR_COMMA);
                    break;
                case Constants.OBJ_LINEITEM:
                    arrSelectColumn = Constants.OBJ_LINEITEM_SELECTFIELD.Split(Constants.CHAR_COMMA);
                    break;
                case Constants.OBJ_DAILYEXCHANGERATE:
                    arrSelectColumn = Constants.OBJ_DAILYEXCHANGERATE_SELECTFIELD.Split(Constants.CHAR_COMMA);
                    break;

                default:
                    break;
            }
            return arrSelectColumn;
        }

        /// <summary>
        /// Method use for Get Request configuration.
        /// </summary>
        /// <param name="objectName"></param>
        /// <param name="externalFilterId"></param>
        /// <param name="apttusFilterId"></param>
        /// <returns>requestConfigModel</returns>
        public static RequestConfigModel GetRequestConfiguration(string objectName, string externalFilterId, string apttusFilterId)
        {
            RequestConfigModel requestConfigModel = new RequestConfigModel()
            {
                accessToken = GetAuthToken(),
                select = GetSelectList(objectName),
                externalFilterField = externalFilterId,
                apttusFilterField = apttusFilterId,
                objectName = objectName
            };

            return requestConfigModel;
        }

        /// <summary>
        /// Method use for mule header detail.
        /// </summary>
        /// <param name="muleXClientId"></param>
        /// <param name="muleUrl"></param>
        /// <param name="requestJson"></param>
        /// <param name="muleXSourceSystem"></param>
        /// <param name="muleAPIName"></param>
        /// <returns>muleHeaderModel</returns>
        public static MuleHeaderModel GetMuleHeaderDetail(string muleXClientId, string muleUrl, string requestJson, string muleXSourceSystem = null, string muleAPIName = null, string muleXBatchId = null)
        {
            MuleHeaderModel muleHeader = new MuleHeaderModel()
            {
                AccessToken = GetMuleAuthToken(),
                XClientId = muleXClientId,
                MuleUrl = muleUrl,
                HttpContentStr = new StringContent(requestJson, Encoding.UTF8, "application/json")
            };

            if (!string.IsNullOrEmpty(muleXSourceSystem))
                muleHeader.XSourceSystem = muleXSourceSystem;
            if (!string.IsNullOrEmpty(muleAPIName))
                muleHeader.APIName = muleAPIName;
            if (!string.IsNullOrEmpty(muleXBatchId))
                muleHeader.XBatchId = muleXBatchId;

            return muleHeader;
        }

        /// <summary>
        /// Get next higher level hierarchy data from the request
        /// </summary>
        /// <param name="reqContent"></param>
        /// <param name="nextLevelContent"></param>
        /// <param name="parentIdName"></param>
        public static void GetNextHierarchyLevel(List<Dictionary<string, object>> reqContent, List<Dictionary<string, object>> nextLevelContent, string parentIdName)
        {
            //Get the independent data from the hierarchy
            foreach (Dictionary<string, object> request in reqContent)
            {
                if (!request.ContainsKey(parentIdName) || (request.ContainsKey(parentIdName) && !reqContent.Exists(data => Convert.ToString(data[Constants.FIELD_EXTERNALID]) == Convert.ToString(request[parentIdName]))))
                {
                    nextLevelContent.Add(request);
                }
            }

            //Remove independent data from actual request
            foreach (Dictionary<string, object> request in nextLevelContent)
            {
                reqContent.Remove(request);
            }

            //Case-insensitive Content
            nextLevelContent = GetCaseIgnoreDictContent(nextLevelContent);
        }

        /// <summary>
        /// Create response from object
        /// </summary>
        /// <param name="response"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static HttpResponseMessage CreateResponse(Dictionary<string, object> response, HttpStatusCode statusCode)
        {
            var reponseMessage = new HttpResponseMessage();
            var jsonMessage = JsonConvert.SerializeObject(response);

            reponseMessage.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");
            reponseMessage.StatusCode = statusCode;

            return reponseMessage;
        }

        /// <summary>
        /// Merge two responses
        /// </summary>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public static HttpResponseMessage CreateResponse(HttpResponseMessage origResponse, HttpResponseMessage newResponse)
        {
            var origResponseString = origResponse.Content.ReadAsStringAsync().Result;
            var newResponseString = newResponse.Content.ReadAsStringAsync().Result;

            if (origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode)
            {
                var origResponseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(origResponseString);
                var newResponseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(newResponseString);

                if (newResponseObj.Inserted != null)
                {
                    origResponseObj.Inserted = origResponseObj.Inserted != null ? origResponseObj.Inserted : new List<dynamic>();
                    origResponseObj.Inserted.AddRange(newResponseObj.Inserted);
                }

                if (newResponseObj.Updated != null)
                {
                    origResponseObj.Updated = origResponseObj.Updated != null ? origResponseObj.Updated : new List<dynamic>();
                    origResponseObj.Updated.AddRange(newResponseObj.Updated);
                }

                if (newResponseObj.Deleted != null)
                {
                    origResponseObj.Deleted = origResponseObj.Deleted != null ? origResponseObj.Deleted : new List<dynamic>();
                    origResponseObj.Deleted.AddRange(newResponseObj.Deleted);
                }

                if (newResponseObj.Errors != null)
                {
                    origResponseObj.Errors = origResponseObj.Errors != null ? origResponseObj.Errors : new List<ErrorInfo>();
                    origResponseObj.Errors.AddRange(newResponseObj.Errors);
                }

                if (origResponseObj != null && (origResponseObj.Inserted != null || origResponseObj.Updated != null || origResponseObj.Deleted != null || origResponseObj.Errors != null))
                    origResponseString = JsonConvert.SerializeObject(origResponseObj);
            }

            var reponseStatusCode = origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode ? origResponse.StatusCode : (!origResponse.IsSuccessStatusCode ? origResponse.StatusCode : newResponse.StatusCode);
            var responseContent = origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode ? origResponseString : (!origResponse.IsSuccessStatusCode ? origResponseString : newResponseString);

            HttpResponseMessage reponseMessage = new HttpResponseMessage();
            reponseMessage.StatusCode = reponseStatusCode;
            reponseMessage.Content = new StringContent(responseContent, Encoding.UTF8, "application/json");

            return reponseMessage;
        }

        /// <summary>
        /// Merge Error Response Only
        /// </summary>
        /// <param name="origResponse"></param>
        /// <param name="newResponse"></param>
        /// <returns></returns>
        public static HttpResponseMessage MergeErrorResponse(HttpResponseMessage origResponse, HttpResponseMessage newResponse)
        {
            var origResponseString = origResponse.Content.ReadAsStringAsync().Result;
            var newResponseString = newResponse.Content.ReadAsStringAsync().Result;

            if (origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode)
            {
                var origResponseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(origResponseString);
                var newResponseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(newResponseString);

                if (newResponseObj.Errors != null)
                {
                    origResponseObj.Errors = origResponseObj.Errors != null ? origResponseObj.Errors : new List<ErrorInfo>();
                    origResponseObj.Errors.AddRange(newResponseObj.Errors);
                }

                if (origResponseObj != null && origResponseObj.Errors != null)
                    origResponseString = JsonConvert.SerializeObject(origResponseObj);
            }

            var reponseStatusCode = origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode ? origResponse.StatusCode : (!origResponse.IsSuccessStatusCode ? origResponse.StatusCode : newResponse.StatusCode);
            var responseContent = origResponse.IsSuccessStatusCode && newResponse.IsSuccessStatusCode ? origResponseString : (!origResponse.IsSuccessStatusCode ? origResponseString : newResponseString);

            HttpResponseMessage reponseMessage = new HttpResponseMessage();
            reponseMessage.StatusCode = reponseStatusCode;
            reponseMessage.Content = new StringContent(responseContent, Encoding.UTF8, "application/json");

            return reponseMessage;
        }

        /// <summary>
        /// Create string json content from string
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static StringContent CreateJsonContent(string content)
        {
            return new StringContent(content, Encoding.UTF8, "application/json");
        }

        /// <summary>
        /// Remove unresolved content from request
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="errors"></param>
        public static void RemoveUnresolvedRequests(List<Dictionary<string, object>> dictContent, List<ErrorInfo> errors, string keyName)
        {
            foreach (ErrorInfo error in errors)
            {
                dictContent.RemoveAll(data => data.ContainsKey(keyName) && error.Record != null && error.Record.ContainsKey(keyName) && data[keyName] == error.Record[keyName]);
            }
        }

    }
}
