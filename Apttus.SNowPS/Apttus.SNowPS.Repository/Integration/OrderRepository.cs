﻿/****************************************************************************************
@Name: OrderRepository.cs
@Author: Meera Kant
@CreateDate: 10 Oct 2017
@Description: Gets Order Details 
@UsedBy: This will be used by API to get Order Details

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Apttus.SNowPS.Respository
{
	public class OrderRepository
	{
		/// <summary>
		/// Get Quote Detail with Order Header, Order Lines & Invoice Schedules
		/// </summary>
		/// <param name="OrderId"></param>
		/// <param name="accessToken"></param>
		/// <returns></returns>
		public string GetOrderDetail(string OrderId, string accessToken)
		{
			var orderJson = string.Empty;
			//Create AQL Query
			try
			{
				var query = new Query(Constants.OBJ_ORDER);
				query.AddColumns(Constants.OBJ_FIELDORDER.Split(Constants.CHAR_COMMA));
				Expression exp = new Expression(ExpressionOperator.AND);
				exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, OrderId));
				query.SetCriteria(exp);
				Join orderLineItem = new Join(Constants.OBJ_ORDER, Constants.OBJ_CPQ_ORDERLINEITEM, Constants.FIELD_ID, Constants.FIELD_ORDERID, JoinType.LEFT);
				orderLineItem.EntityAlias = Constants.OBJALIAS_ORDERLINEITEM;
				query.AddJoin(orderLineItem);
				Join orderInvoiceSchedule = new Join(Constants.OBJ_ORDER, Constants.OBJ_EXT_INVOICESCHEDULE, Constants.FIELD_QUOTEID, Constants.FIELD_EXT_QUOTE, JoinType.LEFT);
				//Join orderInvoiceSchedule = new Join(Constants.OBJ_ORDER, Constants.OBJ_EXT_INVOICESCHEDULE, Constants.FIELD_ID, Constants.FIELD_EXT_ORDERID, JoinType.LEFT);
				orderInvoiceSchedule.EntityAlias = Constants.OBJALIAS_INVOICESCEDULE;
				query.AddJoin(orderInvoiceSchedule);
				var jsonQuery = query.Serialize();

				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = accessToken;
				reqConfig.searchType = SearchType.AQL;
				reqConfig.objectName = Constants.OBJ_ORDER;

				var response = Utilities.Search(jsonQuery, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
					var orderData = JsonConvert.DeserializeObject<List<ResponseOrder>>(responseString);

					//Get json request for Mule
					if (orderData != null && orderData.Count > 0)
						orderJson = CreateOrderHierarchy(orderData);
				}
				return orderJson;
			}
			catch (Exception ex)
			{
				//TODO
				var err = ex.Message;
				var err1 = ex.StackTrace;
				return string.Empty;
			}
		}

		/// <summary>
		/// Build Json Structure for Mule
		/// </summary>
		/// <param name="orderData"></param>
		/// <returns></returns>
		public string CreateOrderHierarchy(List<ResponseOrder> orderData)
		{
			Order objOrder = orderData.Select(x => new Order()
			{
				Id = x.Id,
				Name = x.Name,
				ext_QuoteType = x.ext_QuoteType != null ? x.ext_QuoteType.Key : null,
				PONumber = x.PONumber,
				//ActivatedDate = x.ActivatedDate,
				OrderStartDate = x.OrderStartDate,
				OrderEndDate = x.OrderEndDate,
				ext_SellingEntityCode = x.ext_SellingEntityCode,
				ext_OpportunityNumber = x.ext_OpportunityNumber,
				ext_BaseCurrency = x.ext_BaseCurrency,
				ext_OrderARNotes = x.ext_OrderARNotes,
				ext_AgreementSignedDate = x.ext_AgreementSignedDate,
				ext_datacenter = x.ext_datacenter != null ? x.ext_datacenter.Key : null,
				ext_salestype = x.ext_salestype != null ? x.ext_salestype.Key : null,
				PriceListId = x.PriceListId != null ? x.PriceListId.Name : null,
				ext_TotalNewRenewalACV = x.ext_TotalNewRenewalACV,
				ext_NetPrice = x.ext_NetPrice,
				ext_USDTotalNetNewACV = x.ext_USDTotalNetNewACV,
				ext_taxexempt = x.ext_taxexempt,
				ext_PrimarySalesRepID = x.ext_PrimarySalesRepID,
				ext_SignificantPenalty = x.ext_SignificantPenalty,
				ext_PenaltyAmt = x.ext_PenaltyAmt,
				ext_NoticePeriod = x.ext_NoticePeriod,
				ext_RighttoRefund = x.ext_RighttoRefund,
				ext_RefundPercentage = x.ext_RefundPercentage,
				ext_PSTerritoryXRefID = x.ext_PSTerritoryXRefID,
				ext_SalesTerritoryXRefID = x.ext_SalesTerritoryXRefID,
			}).FirstOrDefault();

			if (objOrder != null)
			{
				objOrder.AD = orderData.Select(x => new AgreementDetails()
				{
					ext_ReferenceContract = x.ext_ReferenceContract,
					ext_AgreementStartDate = x.ext_AgreementStartDate,
					ext_AgreementActivatedDate = x.ext_AgreementActivatedDate,
					PaymentTermId = x.PaymentTermId != null ? x.PaymentTermId.Name : null,
					ext_ApttusParentContractNo = x.ext_ApttusParentContractNo,
					ext_ContractType = x.ext_ContractType,
				}).FirstOrDefault();
			}

			//if (objOrder != null)
			//{
			//    objOrder.SD = orderData.Select(x => new SalesDetails()
			//    {
			//    }).FirstOrDefault();
			//}

			var accountDetails = orderData.Select(x => new AccountDetails()
			{
				ext_SoldToMDMID = x.ext_SoldToMDMID,
				ext_BillToMDMID = x.ext_BillToMDMID,
				ext_AccountMDMID = x.ext_AccountMDMID,
				ext_AffiliatedToCustomer = x.ext_AffiliatedToCustomer,
				ext_partnershiptype = x.ext_partnershiptype != null ? x.ext_partnershiptype.Key : null,
				ext_BillingContact = x.ext_BillingContact,
			});
			if (objOrder != null)
			{
				objOrder.ACD = orderData.Select(x => new AccountDetails()
				{
					ext_SoldToMDMID = x.ext_SoldToMDMID,
					ext_BillToMDMID = x.ext_BillToMDMID,
					ext_AccountMDMID = x.ext_AccountMDMID,
					ext_AffiliatedToCustomer = x.ext_AffiliatedToCustomer,
					ext_partnershiptype = x.ext_partnershiptype != null ? x.ext_partnershiptype.Key : null,
					ext_BillingContact = x.ext_BillingContact,
				}).FirstOrDefault();
			}

			if (objOrder != null && !string.IsNullOrEmpty(orderData.Select(x => x.OLI.Id).FirstOrDefault()))
			{
				var orderLine = orderData.Select(x => new OrderLineItem()
				{
					Id = x.OLI.Id,
					Name = x.OLI.Name,
					ParentBundleNumber = x.OLI.ParentBundleNumber,
					ItemSequence = x.OLI.ItemSequence,
					LineNumber = x.OLI.LineNumber,
					PrimaryLineNumber = x.OLI.PrimaryLineNumber,
					Quantity = x.OLI.Quantity,
					Uom = x.OLI.Uom != null ? x.OLI.Uom.Key : null,
					ListPrice = x.OLI.ListPrice,
					ExtendedPrice = x.OLI.ExtendedPrice,
					NetAdjustmentPercent = x.OLI.NetAdjustmentPercent,
					AdjustmentAmount = x.OLI.AdjustmentAmount,
					NetPrice = x.OLI.NetPrice,
					StartDate = x.OLI.StartDate,
					EndDate = x.OLI.EndDate,
					Term = x.OLI.Term,
					ext_ProductCode = x.OLI.ext_ProductCode,
					ext_ProductDescription = x.OLI.ext_ProductDescription,
					ext_Deployment = x.OLI.ext_Deployment,
					ext_NetNewACV = x.OLI.ext_NetNewACV,
					ext_RenewalLineACV = x.OLI.ext_RenewalLineACV,
					ext_ItemTCV = x.OLI.ext_ItemTCV,
					BillingFrequency = x.OLI.BillingFrequency != null ? x.OLI.BillingFrequency.Key : null,
					ext_TaskID = x.OLI.ext_TaskID,
					ext_Datacenter = x.OLI.ext_Datacenter,
				}).ToList();
				if (orderLine != null && orderLine.Count > 0)
				{
					objOrder.OLI = new List<OrderLineItem>();
					List<OrderLineItem> objDistinct = orderLine.GroupBy(x => x.Id).Select(y => y.First()).ToList();
					objOrder.OLI.AddRange(objDistinct);
				}
			}
			if (objOrder != null && !string.IsNullOrEmpty(orderData.Select(x => x.ISS.Id).FirstOrDefault()))
			{
				var orderInvoiceSchedule = orderData.Select(x => new InvoiceSchedule()
				{
					Id = x.ISS.Id,
					Name = x.ISS.Name,
					ext_Quote = x.ISS.ext_Quote != null ? x.ISS.ext_Quote.Id : null,
					ext_Amount = x.ISS.ext_Amount,
					ext_GrandTotal = x.ISS.ext_GrandTotal,
					ext_EstimatedTax = x.ISS.ext_EstimatedTax,
					ext_InvoiceDisplay = x.ISS.ext_InvoiceDisplay,
					ext_OrderSequence = x.ISS.ext_OrderSequence,
					ext_InvoiceType = x.ISS.ext_InvoiceType,
					ext_InvoiceSchedule = x.ISS.ext_InvoiceSchedule,
					ext_InvoiceDate = x.ISS.ext_InvoiceDate
				}).ToList();

				if (orderInvoiceSchedule != null && orderInvoiceSchedule.Count > 0)
				{
					objOrder.ISS = new List<InvoiceSchedule>();
					List<InvoiceSchedule> objDistinct = orderInvoiceSchedule.GroupBy(x => x.Id).Select(y => y.First()).ToList();
					objOrder.ISS.AddRange(objDistinct);
				}
			}
			//Get json
			return JsonConvert.SerializeObject(objOrder);
		}

		#region PS
		/// <summary>        /// 
		/// Get Order data by Order Id
		/// </summary>
		/// <param name="OrderId"></param>
		/// <param name="accessToken"></param>
		/// <returns></returns>
		public ResponseOrder GetOrderByOrderId(string OrderId, string accessToken)
		{
			try
			{
				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = Utilities.GetAuthToken();
				reqConfig.select = Constants.OBJ_FIELDORDER_HI_SAP.Split(Constants.CHAR_COMMA);
				reqConfig.externalFilterField = Constants.FIELD_ID;
				reqConfig.apttusFilterField = Constants.FIELD_ID;
				reqConfig.objectName = Constants.OBJ_ORDER;

				var dictContent = new List<Dictionary<string, object>>();
				dictContent.Add(new Dictionary<string, object>() { { Constants.FIELD_ID, OrderId } });

				var response = Utilities.Search(dictContent, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
					var orderData = JsonConvert.DeserializeObject<List<ResponseOrder>>(responseString).SingleOrDefault();
					return orderData;
				}
				return null;
			}
			catch (Exception ex)
			{
				//TODO
				return null;
			}
		}

		/// <summary>
		/// Get Order Line Items by Order Id
		/// </summary>
		/// <param name="OrderId"></param>
		/// <param name="accessToken"></param>
		/// <returns></returns>
		public List<OLI> GetOrderLineItemsByOrderId(string OrderId, string accessToken)
		{
			try
			{
				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = Utilities.GetAuthToken();
				reqConfig.select = Constants.OBJ_FIELDORDER_LINEITEM_HI_SAP.Split(Constants.CHAR_COMMA);
				reqConfig.externalFilterField = Constants.FIELD_ORDERID;
				reqConfig.apttusFilterField = Constants.FIELD_ORDERID;
				reqConfig.objectName = Constants.OBJ_CPQ_ORDERLINEITEM;

				var dictContent = new List<Dictionary<string, object>>();
				dictContent.Add(new Dictionary<string, object>() { { Constants.FIELD_ORDERID, OrderId } });

				var response = Utilities.Search(dictContent, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
					var orderLineItemsData = JsonConvert.DeserializeObject<List<OLI>>(responseString);
					return orderLineItemsData;
				}
				return null;
			}
			catch (Exception ex)
			{
				//TODO
				return null;
			}
		}

		/// <summary>
		/// Save updated Order data through API
		/// </summary>
		/// <param name="order">List of Order</param>
		/// <returns>HttpResponseMessage post save API</returns>
		public HttpResponseMessage UpdateOrderData(List<dynamic> order, string accessToken)
		{
			var reqConfig = new RequestConfigModel();
			reqConfig.accessToken = accessToken;
			reqConfig.objectName = Constants.OBJ_ORDER;
			var responseString = Utilities.Update(order, reqConfig);
			return responseString;
		}

		/// <summary>
		/// Save updated Order Line Items data through API
		/// </summary>
		/// <param name="orderLineItems">List of Order Line Items</param>
		/// <returns>HttpResponseMessage post save API</returns>
		public HttpResponseMessage UpdateOrderLineItemsData(List<dynamic> orderLineItems, string accessToken)
		{
			var reqConfig = new RequestConfigModel();
			reqConfig.accessToken = accessToken;
			reqConfig.objectName = Constants.OBJ_CPQ_ORDERLINEITEM;
			var responseString = Utilities.Update(orderLineItems, reqConfig);
			return responseString;
		}
		#endregion PS
		public string GetOrderDetailforHi(string OrderId, string accessToken)
		{
			var orderJson = string.Empty;
			//Create AQL Query to get Order & Order Line items
			try
			{
				var query = new Query(Constants.OBJ_ORDER);
				query.AddColumns(Constants.OBJ_ORDERHI_SELECTED.Split(Constants.CHAR_COMMA));
				Expression exp = new Expression(ExpressionOperator.AND);
				exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, OrderId));
				query.SetCriteria(exp);
				Join orderLineItem = new Join(Constants.OBJ_ORDER, Constants.OBJ_CPQ_ORDERLINEITEM, Constants.FIELD_ID, Constants.FIELD_ORDERID, JoinType.LEFT);
				orderLineItem.EntityAlias = Constants.OBJALIAS_ORDERLINEITEM;
				Expression expJoin = new Expression(ExpressionOperator.AND);
				expJoin.AddCondition(new Condition(Constants.STR_EXT_HIINTEGRATIONPROCESSED, FilterOperator.Equal, false));
				orderLineItem.JoinCriteria = expJoin;
				query.AddJoin(orderLineItem);
				var jsonQuery = query.Serialize();


				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = accessToken;
				reqConfig.searchType = SearchType.AQL;
				reqConfig.objectName = Constants.OBJ_ORDER;

				var response = Utilities.Search(jsonQuery, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
					var orderData = JsonConvert.DeserializeObject<List<ResponseOrder>>(responseString);

					//Get json request for Mule
					if (orderData != null && orderData.Count > 0)
						orderJson = CreateOrderHierarchyforHi(orderData, accessToken);
				}
				return orderJson;
			}
			catch (Exception ex)
			{
				//TODO
				var err = ex.Message;
				var err1 = ex.StackTrace;
				return string.Empty;
			}
		}

		public string CreateOrderHierarchyforHi(List<ResponseOrder> orderData, string accessToken)
		{

			string[] lstOptionId =
				orderData.Where(x => x.OLI != null && !string.IsNullOrEmpty(x.OLI.Id) && x.OLI.OptionId != null && !string.IsNullOrEmpty
				(x.OLI.OptionId.Id)).Select(x => x.OLI.OptionId.Id).ToArray();
			var Plugin = lstOptionId != null && lstOptionId.Length > 0 ? GetPlugins(lstOptionId, accessToken) : null;

			var OrderLineItems = orderData.Where(x => x.OLI != null && !string.IsNullOrEmpty(x.OLI.Id)).Select(
				x => new
				{
					id = x.OLI.Id,
					parentBundleNumber = x.OLI.ParentBundleNumber,
					lineNumber = x.OLI.LineNumber,
					itemSequence = x.OLI.ItemSequence,
					primaryLineNumber = x.OLI.PrimaryLineNumber,
					instanceName = x.OLI.ext_Instancename,
					productName = x.OLI.OptionId != null ? x.OLI.OptionId.Name : null,
					productID = x.OLI.ProductId != null ? x.OLI.ProductId.Id : null,
					plugins = x.OLI.OptionId != null && !string.IsNullOrEmpty(x.OLI.OptionId.Id) && x.OLI.OptionId.ConfigurationType != null && x.OLI.OptionId.ConfigurationType.Equals(Constants.STR_OPTION) && Plugin != null && Plugin.Count > 0 &&
					Plugin.Any(y => y.ext_LKP != null && !string.IsNullOrEmpty(y.ext_LKP.Id) && y.ext_LKP.Id == x.OLI.OptionId.Id) == true ? Plugin.Where(y => y.ext_LKP != null && !string.IsNullOrEmpty(y.ext_LKP.Id) && y.ext_LKP.Id == x.OLI.OptionId.Id).Select(y => new { id = y.ext_PluginId, name = y.ext_PluginName }) : null
				}).ToList();



			var objOrder = orderData.Select(x => new
			{
				id = x.Id,
				number = x.OriginalOrderNumber,
				mdmID = x.ext_AccountMDMID,
				contractType = x.ext_ContractType,
				dataCenter = x.ext_datacenter != null ? x.ext_datacenter.Key : string.Empty,
				OrderLineItems = OrderLineItems != null && OrderLineItems.Count > 0 ? OrderLineItems : null
			}).FirstOrDefault();
			//Get json
			return JsonConvert.SerializeObject(objOrder);
		}

		public List<HiPlugins> GetPlugins(string[] lstOptionId, string accessToken)
		{
			var query = new Query(Constants.OBJ_SNPLUGINS);
			query.AddColumns(Constants.OBJ_SNPLUGINS_SELECTED.Split(Constants.CHAR_COMMA));
			Expression exp = new Expression(ExpressionOperator.AND);
			exp.AddCondition(new Condition(Constants.FIELD_EXT_LKP, FilterOperator.In, lstOptionId));
			exp.AddCondition(new Condition(Constants.FIELD_EXT_AUTOMATICALLYONOFF, FilterOperator.Equal, true));
			query.SetCriteria(exp);
			string jsonQuery = query.Serialize();

			var reqConfig = new RequestConfigModel();
			reqConfig.accessToken = accessToken;
			reqConfig.searchType = SearchType.AQL;
			reqConfig.objectName = Constants.OBJ_SNPLUGINS;

			var response = Utilities.Search(jsonQuery, reqConfig);

			if (response != null && response.IsSuccessStatusCode)
			{
				var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
				return JsonConvert.DeserializeObject<List<HiPlugins>>(responseString);
			}
			return null;
		}

	}
}