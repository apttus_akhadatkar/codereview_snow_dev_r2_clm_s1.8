﻿/****************************************************************************************
@Name: UserRepository.cs
@Author: Mahesh Patel
@CreateDate: 15 Oct 2017
@Description: Contains operations related to User Object 
@UsedBy: This will be used by Apttus to perform operations on User object

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using Apttus.DataAccess.Common.Model;
using Newtonsoft.Json.Linq;
using Apttus.DataAccess.Common.Enums;

namespace Apttus.SNowPS.Repository.Integration
{
    public class UserRepository
    {
        private static string _accessToken = null;

        /// <summary>
        /// Upsert User
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertUsers(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                _accessToken = accessToken;
                HttpResponseMessage responseUser = null;
                var responseMessageUser = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                while (dictContent != null && dictContent.Count > 0 && responseMessageUser.IsSuccessStatusCode)
                {
                    var errors = new List<Model.ErrorInfo>();
                    var roleErrors = new List<Model.ErrorInfo>();

                    //Get the users for next hierarchy
                    var users = new List<Dictionary<string, object>>();
                    Utilities.GetNextHierarchyLevel(dictContent, users, Constants.FIELD_EXT_MANAGERSYSID);

                    //Get User Roles
                    var userContent = new List<Dictionary<string, object>>();
                    GetUserRoles(users, userContent);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = _accessToken;
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                    //Resolve Surf User/Emp Id with Apttus User Id
                    reqConfig.objectName = Constants.OBJ_USER;
                    reqConfig.externalFilterField = Constants.FIELD_EXT_MANAGERSYSID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_EXT_MANAGERSYSID;

                    Utilities.ResolveExternalID(users, reqConfig, errors);

                    //Resolve Timezone Id with Apttus Timezone Id
                    reqConfig.objectName = Constants.OBJ_TIMEZONE;
                    reqConfig.externalFilterField = Constants.FIELD_TIMEZONEID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_TIMEZONEID;

                    Utilities.ResolveExternalID(users, reqConfig, errors);

                    //Remove unresolved requests
                    Utilities.RemoveUnresolvedRequests(users, errors, Constants.FIELD_EXTERNALID);

                    if (users != null && users.Count > 0)
                    {
                        //Upsert User
                        reqConfig.objectName = Constants.OBJ_USER;
                        reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                        responseMessageUser = Utilities.Upsert(users, reqConfig);

                        //Create Response by adding custom errors
                        responseMessageUser = Utilities.CreateResponse(responseMessageUser, errors);

                        //User Role Upsert
                        if (responseMessageUser.IsSuccessStatusCode && (userContent != null && userContent.Count > 0))
                        {
                            var responseMessageRole = UpsertUserRoles(userContent);

                            //Merge responses
                            responseMessageUser = Utilities.MergeErrorResponse(responseMessageUser, responseMessageRole);
                        }
                    }
                    else
                    {
                        var bulkResponseModel = new BulkOperationResponseModel();
                        var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                        responseMessageUser.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");

                        //Create Response by adding custom errors
                        responseMessageUser = Utilities.CreateResponse(responseMessageUser, errors);
                    }

                    //Merge Response
                    if (responseUser == null)
                        responseUser = responseMessageUser;
                    else
                        responseUser = Utilities.CreateResponse(responseUser, responseMessageUser);
                }

                return responseUser;
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Insert/Update/Delete User Roles
        /// </summary>
        /// <param name="users"></param>
        /// <param name="errors"></param>
        private HttpResponseMessage UpsertUserRoles(List<Dictionary<string, object>> userContent)
        {
            var responseMessageRole = new HttpResponseMessage();
            var errors = new List<Model.ErrorInfo>();

            //Resolve Surf User/Emp Id with Apttus User Id
            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = _accessToken;
            reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

            reqConfig.objectName = Constants.OBJ_USER;
            reqConfig.externalFilterField = Constants.FIELD_CRM_USERID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_CRM_USERID;

            Utilities.ResolveExternalID(userContent, reqConfig, errors);

            //Get RoleID from Role
            reqConfig.objectName = Constants.OBJ_ROLE;
            reqConfig.externalFilterField = Constants.FIELD_CMN_ROLEID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_CMN_ROLEID;

            Utilities.ResolveExternalID(userContent, reqConfig, errors);

            //Remove unresolved requests
            Utilities.RemoveUnresolvedRequests(userContent, errors, Constants.FIELD_NAME);

            //Upsert Role
            if (userContent != null && userContent.Count > 0)
            {
                reqConfig.objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                responseMessageRole = Utilities.Upsert(userContent, reqConfig);
                responseMessageRole = Utilities.CreateResponse(responseMessageRole, errors);
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                responseMessageRole.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");

                //Create Response by adding custom errors
                responseMessageRole = Utilities.CreateResponse(responseMessageRole, errors);
            }

            return responseMessageRole;
        }

        /// <summary>
        /// Get User Roles from Request
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="userContent"></param>
        private void GetUserRoles(List<Dictionary<string, object>> dictContent, List<Dictionary<string, object>> userContent)
        {
            //Content for User Role
            var defaultRole = Convert.ToString(ConfigurationManager.AppSettings["DefaultRole"]);

            foreach (Dictionary<string, object> user in dictContent)
            {
                if (user.ContainsKey(Constants.NODE_ROLE) && user.ContainsKey(Constants.FIELD_EXTERNALID))
                {
                    var roles = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(Convert.ToString(user["Role"]));

                    foreach (var role in roles)
                    {
                        if (role != null && role.Count > 0)
                        {
                            role.Add(Constants.FIELD_CRM_USERID, Convert.ToString(user[Constants.FIELD_EXTERNALID]));

                            userContent.Add(role);
                        }
                    }
                    user.Remove(Constants.NODE_ROLE);
                }
                else
                {
                    //Assign default role if no role provided for new user
                    if (Utilities.IsNewUser(user, _accessToken))
                    {
                        Dictionary<string, object> userDict = new Dictionary<string, object>();
                        userDict.Add(Constants.FIELD_EXTERNALID, Convert.ToString(user[Constants.FIELD_EXTERNALID]));
                        userDict.Add(Constants.FIELD_CRM_USERID, Convert.ToString(user[Constants.FIELD_EXTERNALID]));
                        userDict.Add(Constants.FIELD_CMN_ROLEID, defaultRole);

                        if (user.ContainsKey(Constants.FIELD_NAME))
                            userDict.Add(Constants.FIELD_NAME, Convert.ToString(user[Constants.FIELD_NAME] + "-" + defaultRole));

                        userContent.Add(userDict);
                    }
                }
            }

            userContent = Utilities.GetCaseIgnoreDictContent(userContent);
        }



        #region PS

        /// <summary>
        /// Remove user role On Update Stakeholder
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        public HttpResponseMessage RemoveUserRoleOnUpdateStakeholder(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                bool isAnyIncativeUser = false;
                foreach (Dictionary<string, object> user in dictContent)
                {
                    if (!Convert.ToBoolean(user["ext_IsActive"]))
                        isAnyIncativeUser = true;
                }

                if (!isAnyIncativeUser)
                {
                    new HttpResponseMessage
                    {
                        Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                        StatusCode = System.Net.HttpStatusCode.OK
                    };
                }
                var query = new Query(Constants.OBJ_CMN_PARTICIPANT);
                query.AddColumns(Constants.OBJ_CMN_PARTICIPANT_SELECTFIELD.Split(Constants.CHAR_COMMA));

                Join participantRole = new Join(Constants.OBJ_CMN_PARTICIPANT, Constants.OBJ_CMN_PARTICIPANTROLE, Constants.FIELD_ID, Constants.FIELD_PARTICIPANTID, JoinType.INNER, Constants.OBJ_CMN_PARTICIPANTROLE);
                query.AddJoin(participantRole);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_CMN_PARTICIPANT;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var participantData = JsonConvert.DeserializeObject<List<ParticipantPSModel>>(responseString);

                    List<Dictionary<string, object>> removeUserRoleList = new List<Dictionary<string, object>>();
                    foreach (Dictionary<string, object> user in dictContent)
                    {
                        if (!Convert.ToBoolean(user["ext_IsActive"]))
                        {
                            string roleId = GetRoleId(Convert.ToString(user["Role"]), accessToken);
                            participantData = participantData.Where(a => a.Role.Id == roleId && a.Participant.Id == Convert.ToString(user["UserID"]) && a.Participant.Type == Constants.STR_CRM_USER && a.ContextObject.Type == Constants.STR_CPQ_QUOTE).ToList();
                            if (participantData.Count == 0)
                            {
                                Dictionary<string, object> userRole = new Dictionary<string, object>();
                                userRole.Add("UserId", Convert.ToString(user["UserID"]));
                                userRole.Add("RoleId", roleId);
                                removeUserRoleList.Add(userRole);
                            }
                        }
                    }

                    if (removeUserRoleList.Count > 0)
                    {
                        var existUserRoleList = GetUserRole(accessToken);
                        var deleteUserRoleList = new List<Dictionary<string, object>>();

                        foreach (Dictionary<string, object> obj in removeUserRoleList)
                        {
                            var userRoleList = existUserRoleList.Where(d => d.RoleId.Id == Convert.ToString(obj["RoleId"]) && d.UserId.Id == Convert.ToString(obj["UserId"])).ToList();
                            foreach (var userRole in userRoleList)
                            {
                                Dictionary<string, object> deleteUserRole = new Dictionary<string, object>();
                                deleteUserRole.Add("Id", userRole.Id);
                                deleteUserRoleList.Add(deleteUserRole);
                            }
                        }
                        var deleteres = Utilities.DeleteMultiple(deleteUserRoleList, new RequestConfigModel
                        {
                            accessToken = accessToken,
                            objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER,
                        });
                    }
                }

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.OK
                };
                return resp;
            }
            catch (Exception ex)
            {

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
                return resp;
            }
        }

        /// <summary>
        /// Add User Role On Update Stakeholder
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        public HttpResponseMessage AddUserRoleOnUpdateStakeholder(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                bool isAnyAtiveUser = false;
                foreach (Dictionary<string, object> user in dictContent)
                {
                    if (Convert.ToBoolean(user["ext_IsActive"]))
                        isAnyAtiveUser = true;
                }

                if (!isAnyAtiveUser)
                {
                    new HttpResponseMessage
                    {
                        Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                        StatusCode = System.Net.HttpStatusCode.OK
                    };
                }

                var query = new Query(Constants.OBJ_CMN_PARTICIPANT);
                query.AddColumns(Constants.OBJ_CMN_PARTICIPANT_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Join participantRole = new Join(Constants.OBJ_CMN_PARTICIPANT, Constants.OBJ_CMN_PARTICIPANTROLE, Constants.FIELD_ID, Constants.FIELD_PARTICIPANTID, JoinType.INNER, Constants.OBJ_CMN_PARTICIPANTROLE);
                query.AddJoin(participantRole);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_CMN_PARTICIPANT;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var participantData = JsonConvert.DeserializeObject<List<ParticipantPSModel>>(responseString);

                    List<Dictionary<string, object>> addUserRoleList = new List<Dictionary<string, object>>();
                    var existUserRoleList = GetUserRole(accessToken);
                    foreach (Dictionary<string, object> user in dictContent)
                    {

                        if (Convert.ToBoolean(user["ext_IsActive"]))
                        {
                            string roleId = GetRoleId(Convert.ToString(user["Role"]), accessToken);
                            participantData = participantData.Where(a => a.Role.Id == roleId && a.Participant.Id == Convert.ToString(user["UserID"]) && a.Participant.Type == Constants.STR_CRM_USER && a.ContextObject.Type == Constants.STR_CPQ_QUOTE).ToList();
                            if (participantData.Count > 0)
                            {
                                var userRole = existUserRoleList.Where(d => d.RoleId.Id == roleId && d.UserId.Id == Convert.ToString(user["UserID"])).FirstOrDefault();
                                if (userRole == null)
                                {
                                    Dictionary<string, object> addUserRole = new Dictionary<string, object>();
                                    addUserRole.Add("Name", Convert.ToString(user["Role"]));
                                    addUserRole.Add("UserId", Convert.ToString(user["UserID"]));
                                    addUserRole.Add("RoleId", roleId);
                                    addUserRoleList.Add(addUserRole);
                                }
                            }
                        }
                    }

                    if (addUserRoleList.Count > 0)
                    {
                        var userRoleList = new List<dynamic>();

                        foreach (Dictionary<string, object> userRole in addUserRoleList)
                        {
                            userRoleList.Add(new
                            {
                                Name = Convert.ToString(userRole["Name"]),
                                crm_UserId = Convert.ToString(userRole["UserId"]),
                                cmn_RoleId = Convert.ToString(userRole["RoleId"]),
                                ext_IsActive = true
                            });
                        }

                        var responseResult = Utilities.Create(userRoleList, new RequestConfigModel
                        {
                            accessToken = accessToken,
                            objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER,
                        });
                    }
                }

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.OK
                };
                return resp;
            }
            catch (Exception ex)
            {
                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
                return resp;
            }
        }


        /// <summary>
        /// Remove User Role on update User Group Membership
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        public HttpResponseMessage RemoveUserRoleOnUpdateUserGroupMembership(string id, string accessToken)
        {
            try
            {
                var query = new Query(Constants.OBJ_USERGROUPMEMBERSHIP);
                query.AddColumns(Constants.OBJ_CRM_USERGROUPMEMBERSHIP_SELECTFIELD.Split(Constants.CHAR_COMMA));

                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, id));
                query.SetCriteria(exp);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_USERGROUPMEMBERSHIP;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var userGroupMemberData = JsonConvert.DeserializeObject<List<UserGroupMembershipModel>>(responseString).FirstOrDefault();

                    if (!userGroupMemberData.IsActive)
                    {
                        var queryUGMember = new Query(Constants.OBJ_USERGROUPMEMBERSHIP);
                        queryUGMember.AddColumns(Constants.OBJ_CRM_USERGROUPMEMBERSHIP_SELECTFIELD.Split(Constants.CHAR_COMMA));

                        Expression expUGMember = new Expression(ExpressionOperator.AND);
                        expUGMember.AddCondition(new Condition(Constants.FIELD_EXT_ROLEID, FilterOperator.Equal, userGroupMemberData.RoleId.Id));
                        expUGMember.AddCondition(new Condition(Constants.FIELD_USER_ID, FilterOperator.Equal, userGroupMemberData.UserId.Id));
                        expUGMember.AddCondition(new Condition(Constants.FIELD_EXT_ISACTIVE, FilterOperator.Equal, true));
                        expUGMember.AddCondition(new Condition(Constants.FIELD_USERGROUPID, FilterOperator.NotEqual, userGroupMemberData.UserGroupId.Id));
                        queryUGMember.SetCriteria(expUGMember);

                        var jsonQueryUGMember = queryUGMember.Serialize();
                        var reqConfigUGMember = new RequestConfigModel();
                        reqConfigUGMember.accessToken = accessToken;
                        reqConfigUGMember.searchType = SearchType.AQL;
                        reqConfigUGMember.objectName = Constants.OBJ_USERGROUPMEMBERSHIP;
                        reqConfigUGMember.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                        var responseUGMember = Utilities.Search(jsonQueryUGMember, reqConfigUGMember);

                        if (responseUGMember != null && responseUGMember.IsSuccessStatusCode)
                        {
                            var responseStringUGMember = JObject.Parse(responseUGMember.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                            var resultData = JsonConvert.DeserializeObject<List<UserGroupMembershipModel>>(responseStringUGMember);
                            if (resultData.Count == 0)
                            {
                                var existUserRoleList = GetUserRole(accessToken);
                                var deleteUserRoleList = new List<Dictionary<string, object>>();
                                var userRoleList = existUserRoleList.Where(d => d.RoleId.Id == userGroupMemberData.RoleId.Id && d.UserId.Id == userGroupMemberData.UserId.Id).ToList();
                                foreach (var userRole in userRoleList)
                                {
                                    Dictionary<string, object> deleteUserRole = new Dictionary<string, object>();
                                    deleteUserRole.Add("Id", userRole.Id);
                                    deleteUserRoleList.Add(deleteUserRole);
                                }

                                var deleteres = Utilities.DeleteMultiple(deleteUserRoleList, new RequestConfigModel
                                {
                                    accessToken = accessToken,
                                    objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER,
                                });
                            }
                        }
                    }
                }

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.OK
                };
                return resp;
            }
            catch (Exception ex)
            {

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
                return resp;
            }
        }

        /// <summary>
        /// Add User Role on update User Group Membership
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        public HttpResponseMessage AddUserRoleOnUpdateUserGroupMembership(string id, string accessToken)
        {
            try
            {
                var query = new Query(Constants.OBJ_USERGROUPMEMBERSHIP);
                query.AddColumns(Constants.OBJ_CRM_USERGROUPMEMBERSHIP_SELECTFIELD.Split(Constants.CHAR_COMMA));

                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, id));
                query.SetCriteria(exp);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_USERGROUPMEMBERSHIP;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var userGroupMemberData = JsonConvert.DeserializeObject<List<UserGroupMembershipModel>>(responseString).FirstOrDefault();

                    if (userGroupMemberData.IsActive)
                    {
                        List<Dictionary<string, object>> addUserRoleList = new List<Dictionary<string, object>>();
                        var existUserRoleList = GetUserRole(accessToken);

                        var userRole = existUserRoleList.Where(d => d.RoleId.Id == userGroupMemberData.RoleId.Id && d.UserId.Id == userGroupMemberData.UserId.Id).FirstOrDefault();
                        if (userRole == null)
                        {
                            var addUserRole = new List<dynamic>();
                            addUserRole.Add(new
                            {
                                Name = userGroupMemberData.RoleId.Name,
                                cmn_RoleId = userGroupMemberData.RoleId.Id,
                                crm_UserId = userGroupMemberData.UserId.Id,
                                ext_IsActive = true

                            });
                            var responseResult = Utilities.Create(addUserRole, new RequestConfigModel
                            {
                                accessToken = accessToken,
                                objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER,
                            });
                        }
                    }
                }

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message : Success", Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.OK
                };
                return resp;
            }
            catch (Exception ex)
            {

                var resp = new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
                return resp;
            }
        }

        /// <summary>
        /// Get User Role Junction List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public List<UserRoleModel> GetUserRole(string accessToken)
        {
            try
            {
                var query = new Query(Constants.OBJ_JNC_CMN_ROLE_CRM_USER);
                query.AddColumns(Constants.OBJ_JNC_CMN_ROLE_CRM_USER_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_EXT_ISACTIVE, FilterOperator.Equal, true));
                query.SetCriteria(exp);
                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_JNC_CMN_ROLE_CRM_USER;

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    return JsonConvert.DeserializeObject<List<UserRoleModel>>(responseString);

                }
                return null;
            }
            catch (Exception ex)
            {
                //TODO
                throw ex;
            }
        }

        /// <summary>
        /// Get Role Id
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public string GetRoleId(string roleName, string accessToken)
        {
            try
            {
                var query = new Query(Constants.OBJ_CMN_ROLE);
                query.AddColumns(Constants.OBJ_CMN_ROLE_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_NAME, FilterOperator.Equal, roleName));

                query.SetCriteria(exp);
                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_CMN_ROLE;

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var data = JsonConvert.DeserializeObject<List<RoleModel>>(responseString);
                    var role = data.Where(d => d.Name == roleName).FirstOrDefault();
                    return role.Id;
                }
                return null;
            }
            catch (Exception ex)
            {
                //TODO
                throw ex;
            }
        }
        #endregion PS
    }
}
