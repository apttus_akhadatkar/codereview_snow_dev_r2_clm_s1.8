﻿/****************************************************************************************
@Name: TerritoryRepository.cs
@Author: Mahesh Patel
@CreateDate: 15 Oct 2017
@Description: Contains operations related to Territory Object 
@UsedBy: This will be used by Apttus to perform operations on Territory object

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static Apttus.SNowPS.Common.Enums;

namespace Apttus.SNowPS.Repository.Integration
{
    public class TerritoryRepository
    {
        private static string _accessToken = null;

        /// <summary>
        /// Upsert Territory
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertTerritory(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                HttpResponseMessage responseTerritory = null;
                var responseMessageTerritory = new HttpResponseMessage();
                _accessToken = accessToken;

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                //Basic Request Config
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = _accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                while (dictContent != null && dictContent.Count > 0 && responseMessageTerritory.IsSuccessStatusCode)
                {
                    var errors = new List<Model.ErrorInfo>();

                    //Get the territories for next hierarchy
                    var territories = new List<Dictionary<string, object>>();
                    Utilities.GetNextHierarchyLevel(dictContent, territories, Constants.FIELD_EXT_MDMPARENTID);

                    //Content for Territory User Role
                    var approverUsers = new List<Dictionary<string, object>>();
                    var elevatedUsers = new List<Dictionary<string, object>>();
                    GetUserRoles(territories, approverUsers, elevatedUsers);

                    //Resolve Surf User/Emp Id with Apttus User Id
                    reqConfig.objectName = Constants.OBJ_TERRITORY;
                    reqConfig.externalFilterField = Constants.FIELD_EXT_MDMPARENTID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_EXT_MDMPARENTID;

                    Utilities.ResolveExternalID(territories, reqConfig, errors);

                    //Remove unresolved requests
                    Utilities.RemoveUnresolvedRequests(territories, errors, Constants.FIELD_EXTERNALID);

                    if (territories != null && territories.Count > 0)
                    {
                        //Upsert Territory
                        reqConfig.objectName = Constants.OBJ_TERRITORY;
                        reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                        responseMessageTerritory = Utilities.Upsert(territories, reqConfig);

                        //Create Response by adding custom errors
                        responseMessageTerritory = Utilities.CreateResponse(responseMessageTerritory, errors);

                        //For the territory user mapping
                        if (responseMessageTerritory.IsSuccessStatusCode)
                        {
                            if (approverUsers != null && approverUsers.Count > 0)
                            {
                                errors = new List<Model.ErrorInfo>();

                                var respTerritoryApproverRoles = UpsertTerritoryApproverRoles(approverUsers, errors);
                                //Merge error response
                                responseMessageTerritory = Utilities.MergeErrorResponse(responseMessageTerritory, respTerritoryApproverRoles);
                            }
                            if (elevatedUsers != null && elevatedUsers.Count > 0)
                            {
                                errors = new List<Model.ErrorInfo>();

                                var respTerritoryElevatedRoles = UpsertTerritoryElevatedRoles(elevatedUsers, errors);
                                //Merge error response
                                responseMessageTerritory = Utilities.MergeErrorResponse(responseMessageTerritory, respTerritoryElevatedRoles);
                            }
                        }
                    }
                    else
                    {
                        var bulkResponseModel = new BulkOperationResponseModel();
                        var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                        responseMessageTerritory.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");

                        //Create Response by adding custom errors
                        responseMessageTerritory = Utilities.CreateResponse(responseMessageTerritory, errors);

                        return responseMessageTerritory;
                    }

                    //Merge Responses
                    if (responseTerritory == null)
                        responseTerritory = responseMessageTerritory;
                    else
                        responseTerritory = Utilities.CreateResponse(responseTerritory, responseMessageTerritory);
                }

                return responseTerritory;
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Upsert Approver Users of Territory
        /// </summary>
        /// <param name="userContent"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private HttpResponseMessage UpsertTerritoryApproverRoles(List<Dictionary<string, object>> userContent, List<Model.ErrorInfo> errors)
        {
            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = _accessToken;
            reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

            //Resolve Surf User/Emp Id with Apttus User Id
            reqConfig.objectName = Constants.OBJ_USER;
            reqConfig.externalFilterField = Constants.FIELD_EXT_USERID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_USERID;

            Utilities.ResolveExternalID(userContent, reqConfig, errors);

            //Get RoleID from Role
            reqConfig.objectName = Constants.OBJ_ROLE;
            reqConfig.externalFilterField = Constants.FIELD_EXT_ROLEID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_ROLEID;

            Utilities.ResolveExternalID(userContent, reqConfig, errors);

            //Resolve Surf Territory Id with Apttus Territory Id
            reqConfig.objectName = Constants.OBJ_TERRITORY;
            reqConfig.externalFilterField = Constants.FIELD_EXT_TERRITORYID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_TERRITORYID;

            Utilities.ResolveExternalID(userContent, reqConfig, errors);

            //Remove unresolved requests
            Utilities.RemoveUnresolvedRequests(userContent, errors, Constants.FIELD_EXTERNALID);

            //Remove Created ExternalID
            foreach (var user in userContent)
            {
                user.Remove(Constants.FIELD_TERRITORYEXTID);
            }

            if (userContent != null && userContent.Count > 0)
            {
                //Upsert Territory User Mapping
                reqConfig.objectName = Constants.OBJ_JNC_EXT_TERRITORY_CRM_USER;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                var respTerritoryRoles = Utilities.Upsert(userContent, reqConfig);

                return Utilities.CreateResponse(respTerritoryRoles, errors);
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                var respTerritoryRoles = new HttpResponseMessage();
                respTerritoryRoles.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");

                //Create Response by adding custom errors
                return Utilities.CreateResponse(respTerritoryRoles, errors);
            }
        }

        /// <summary>
        /// Upsert Elevated Access Users of Territory
        /// </summary>
        /// <param name="elevatedUsers"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private HttpResponseMessage UpsertTerritoryElevatedRoles(List<Dictionary<string, object>> elevatedUsers, List<Model.ErrorInfo> errors)
        {
            var userGroup = new List<Dictionary<string, object>>();
            var userGroupMemberships = new List<Dictionary<string, object>>();

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = _accessToken;
            reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

            //Resolve MDM Territory Id with Apttus Territory Id
            reqConfig.objectName = Constants.OBJ_TERRITORY;
            reqConfig.externalFilterField = Constants.FIELD_EXT_TERRITORYID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_TERRITORYID;

            Utilities.ResolveExternalID(elevatedUsers, reqConfig, errors);

            //Resolve MDM Parent Territory Id with Apttus Territory Id
            reqConfig.objectName = Constants.OBJ_TERRITORY;
            reqConfig.externalFilterField = Constants.FIELD_EXT_PARENTTERRITORYID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_PARENTTERRITORYID;

            Utilities.ResolveExternalID(elevatedUsers, reqConfig, errors);

            //User Group Data
            foreach (Dictionary<string, object> user in elevatedUsers)
            {
                if (!userGroup.Exists(data => data.ContainsKey(Constants.FIELD_EXT_TERRITORYID) && user.ContainsKey(Constants.FIELD_EXT_TERRITORYID) && data[Constants.FIELD_EXT_TERRITORYID] == user[Constants.FIELD_EXT_TERRITORYID]))
                {
                    var userDict = new Dictionary<string, object>();

                    userDict.Add(Constants.FIELD_EXTERNALID, user[Constants.FIELD_TERRITORYEXTID]);

                    if (user.ContainsKey(Constants.FIELD_NAME))
                        userDict.Add(Constants.FIELD_NAME, Convert.ToString(user[Constants.FIELD_NAME]));

                    if (user.ContainsKey(Constants.FIELD_EXT_PARENTTERRITORYID))
                        userDict.Add(Constants.FIELD_EXT_PARENTTERRITORYID, user[Constants.FIELD_EXT_PARENTTERRITORYID]);

                    if (user.ContainsKey(Constants.FIELD_EXT_TERRITORYID))
                        userDict.Add(Constants.FIELD_EXT_TERRITORYID, user[Constants.FIELD_EXT_TERRITORYID]);

                    userGroup.Add(userDict);
                }
            }

            //Upsert User Group
            reqConfig.objectName = Constants.OBJ_USERGROUP;
            reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
            var respUserGroup = Utilities.Upsert(userGroup, reqConfig);
            respUserGroup = Utilities.CreateResponse(respUserGroup, errors);

            //Resolve User Group External Id with Apttus User Group Id
            reqConfig.objectName = Constants.OBJ_USERGROUP;
            reqConfig.externalFilterField = Constants.FIELD_TERRITORYEXTID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_USERGROUPID;

            Utilities.ResolveExternalID(elevatedUsers, reqConfig, errors);

            //Resolve Surf User/Emp Id with Apttus User Id
            reqConfig.objectName = Constants.OBJ_USER;
            reqConfig.externalFilterField = Constants.FIELD_EXT_USERID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_USER_ID;

            Utilities.ResolveExternalID(elevatedUsers, reqConfig, errors);

            //Resolve Surf Role Id with Apttus Role Id
            reqConfig.objectName = Constants.OBJ_ROLE;
            reqConfig.externalFilterField = Constants.FIELD_EXT_ROLEID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_EXT_ROLEID;

            Utilities.ResolveExternalID(elevatedUsers, reqConfig, errors);

            //Remove unresolved data
            Utilities.RemoveUnresolvedRequests(elevatedUsers, errors, Constants.FIELD_EXTERNALID);

            //User Group Membership Data
            foreach (Dictionary<string, object> user in elevatedUsers)
            {
                if(user.ContainsKey(Constants.FIELD_TERRITORYEXTID))
                    user.Remove(Constants.FIELD_TERRITORYEXTID);
                if(user.ContainsKey(Constants.FIELD_EXT_TERRITORYID))
                    user.Remove(Constants.FIELD_EXT_TERRITORYID);
                if(user.ContainsKey(Constants.FIELD_EXT_PARENTTERRITORYID))
                    user.Remove(Constants.FIELD_EXT_PARENTTERRITORYID);
                if (user.ContainsKey(Constants.FIELD_EXT_USERID))
                    user.Remove(Constants.FIELD_EXT_USERID);
                userGroupMemberships.Add(user);
            }

            if (userGroupMemberships != null && userGroupMemberships.Count > 0)
            {
                //Upsert User Group Membership
                reqConfig.objectName = Constants.OBJ_USERGROUPMEMBERSHIP;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                var respUserGroupMembership = Utilities.Upsert(userGroupMemberships, reqConfig);
                respUserGroupMembership = Utilities.CreateResponse(respUserGroupMembership, errors);

                //Merge reponses
                return Utilities.CreateResponse(respUserGroup, respUserGroupMembership);
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                var respUserGroupMembership = new HttpResponseMessage();
                respUserGroupMembership.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");

                //Create Response by adding custom errors
                return Utilities.CreateResponse(respUserGroupMembership, errors);
            }
        }

        /// <summary>
        /// Get User Roles from Request
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="userContent"></param>
        private void GetUserRoles(List<Dictionary<string, object>> dictContent, List<Dictionary<string, object>> approverUsers, List<Dictionary<string, object>> elevatedUsers)
        {
            //Content for Territory User Role
            foreach (Dictionary<string, object> request in dictContent)
            {
                if (request.ContainsKey(Constants.NODE_USERS) && request.ContainsKey(Constants.FIELD_EXTERNALID))
                {
                    var jsonUsers = JsonConvert.SerializeObject(request[Constants.NODE_USERS]);
                    var dictUsers = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonUsers);

                    foreach (Dictionary<string, object> user in dictUsers)
                    {
                        user.Add(Constants.FIELD_TERRITORYEXTID, request[Constants.FIELD_EXTERNALID]);
                        user.Add(Constants.FIELD_EXT_TERRITORYID, request[Constants.FIELD_EXTERNALID]);

                        if (user.ContainsKey(Constants.FIELD_EXT_APPROVALTYPE) && Convert.ToString(user[Constants.FIELD_EXT_APPROVALTYPE]) == "Approver")
                        {
                            approverUsers.Add(user);
                        }
                        else if (user.ContainsKey(Constants.FIELD_EXT_APPROVALTYPE) && Convert.ToString(user[Constants.FIELD_EXT_APPROVALTYPE]) == "Elevated Access")
                        {
                            if(request.ContainsKey(Constants.FIELD_EXT_MDMPARENTID))
                                user.Add(Constants.FIELD_EXT_PARENTTERRITORYID, Convert.ToString(request[Constants.FIELD_EXT_MDMPARENTID]));

                            elevatedUsers.Add(user);
                        }
                    }
                    request.Remove(Constants.NODE_USERS);
                }
            }
        }
    }
}
