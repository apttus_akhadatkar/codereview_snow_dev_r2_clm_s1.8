﻿/****************************************************************************************
@Name: CustomerRepository.cs
@Author: Mahesh Patel
@CreateDate: 15 Oct 2017
@Description: Contains operations related to Customer Instance Object 
@UsedBy: This will be used by Apttus to perform operations on Customer object

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace Apttus.SNowPS.Repository.Integration
{
    public class CustomerRepository
    {
        /// <summary>
        /// Upsert Customer Instances
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertCustomerInstance(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var errors = new List<ErrorInfo>();
                var responseCustomer = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                //Resolve Account MDM Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_EXT_ACCOUNTMDMID;//"MDM ID";
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;//"ExternalId";
                reqConfig.resolvedID = Constants.FIELD_EXT_ACCOUNTID;//Apttus Lookup Field

                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Upsert Customer
                    reqConfig.objectName = Constants.OBJ_CUSTOMER;
                    reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                    var responseMessage = Utilities.Upsert(dictContent, reqConfig);

                    return Utilities.CreateResponse(responseMessage, errors);
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseCustomer.Content = Utilities.CreateJsonContent(jsonResponse);

                    //Create Response by adding custom errors
                    return Utilities.CreateResponse(responseCustomer, errors);
                }
            }
            catch
            {
                //TODO
                throw;
            }
        }
    }
}
