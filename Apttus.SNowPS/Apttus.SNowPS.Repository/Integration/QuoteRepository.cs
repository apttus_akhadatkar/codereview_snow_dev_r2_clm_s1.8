﻿/****************************************************************************************
@Name: QuoteRepository.cs
@Author: Kiran Satani
@CreateDate: 27 Sep 2017
@Description: Gets Quote Details 
@UsedBy: This will be used by API to get Quote Details

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;
using INTEGRATION = Apttus.SNowPS.Model.Integration;

namespace Apttus.SNowPS.Respository
{
    public class QuoteRepository
    {
        #region Properties
        private string quoteUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        #endregion

        /// <summary>
        /// Upsert Quote
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertQuote(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var errors = new List<Model.ErrorInfo>();
                var responseQuote = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                //Resolve Parent MDM Account Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_EXT_ACCOUNTMDMID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_ACCOUNTID;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve Partner Account Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_PARTNERACCOUNT;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_PARTNERACCOUNT;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve Parent MDM Account Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_ENDCUSTOMERMDMID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_ENDCUSTOMERMDMID;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve Parent MDM Account Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_MSPACCOUNT;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_MSPACCOUNT;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve FieldTerritoryXRef with FieldTerritory
                reqConfig.objectName = Constants.OBJ_TERRITORY;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_FIELDTERRITORY;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_FIELDTERRITORY;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve MSPTerritoryXRef with MSPTerritory
                //reqConfig.objectName = Constants.OBJ_TERRITORY;
                //reqConfig.externalFilterField = Constants.FIELD_EXT_MSPTERRITORY;
                //reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                //reqConfig.resolvedID = Constants.FIELD_EXT_MSPTERRITORY;
                //Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve PSTerritoryXRef with PSTerritory
                reqConfig.objectName = Constants.OBJ_TERRITORY;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_PSTERRITORY;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_PSTERRITORY;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve EducationISR with Apttus User External ID
                reqConfig.objectName = Constants.OBJ_USER;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_EDUCATIONUSER;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_EDUCATIONUSER;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve QuoteOwnerUser with Apttus User External ID
                reqConfig.objectName = Constants.OBJ_USER;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_QUOTEOWNERUSER;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_QUOTEOWNERUSER;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Resolve QuoteOriginatorUser with Apttus User External ID
                reqConfig.objectName = Constants.OBJ_USER;
                reqConfig.externalFilterField = Constants.FIELD_STR_EXT_QUOTEORIGINATORUSER;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_EXT_QUOTEORIGINATORUSER;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Upsert Quote
                    reqConfig.objectName = Constants.OBJ_QUOTE;
                    reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                    var responseMessage = Utilities.Upsert(dictContent, reqConfig);

                    //Create Response
                    return Utilities.CreateResponse(responseMessage, errors);
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseQuote.Content = Utilities.CreateJsonContent(jsonResponse);

                    //Create Response by adding custom errors
                    return Utilities.CreateResponse(responseQuote, errors);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Quote Detail with Quote Header,Quote Lines & Milestones
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public string GetQuoteDetail(string QuoteId, string accessToken)
        {
            var quoteJson = string.Empty;
            //Build AQL Query to fetch Quote,Quote Line Items & MileStones
            try
            {
                var query = new Query(Constants.OBJ_QUOTE);
                query.AddColumns(Constants.OBJ_FIELDQUOTE.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, QuoteId));
                query.SetCriteria(exp);
                Join quoteLineItem = new Join(Constants.OBJ_QUOTE, Constants.OBJ_CPQ_QUOTELINEITEM, Constants.FIELD_ID, Constants.FIELD_QUOTEID, JoinType.LEFT);
                quoteLineItem.EntityAlias = Constants.OBJALIAS_QUOTELINEITEM;
                query.AddJoin(quoteLineItem);
                Join quoteMileStone = new Join(Constants.OBJ_QUOTE, Constants.OBJ_EXT_MILESTONE, Constants.FIELD_ID, Constants.FIELD_EXT_QUOTEID, JoinType.LEFT);
                quoteMileStone.EntityAlias = Constants.OBJALIAS_MILESTONE;
                query.AddJoin(quoteMileStone);
                var jsonQuery = query.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_QUOTE;

                var response = Utilities.Search(jsonQuery, reqConfig);
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var quoteData = JsonConvert.DeserializeObject<List<INTEGRATION.ResponseQuote>>(responseString);

                    //Get json request for Mule
                    if (quoteData != null && quoteData.Count > 0)
                        quoteJson = CreateQuoteHierarchy(quoteData);
                }
                return quoteJson;
            }
            catch (Exception ex)
            {
                //TODO
                var a = ex.Message;
                var b = ex.StackTrace;
                return string.Empty;
            }
        }

        /// <summary>
        /// Build Json Structure for Mule
        /// </summary>
        /// <param name="quoteData"></param>
        /// <returns></returns>
        public string CreateQuoteHierarchy(List<INTEGRATION.ResponseQuote> quoteData)
        {
            //Quote Header Detail
            INTEGRATION.QuoteModel objQuote = quoteData.Select(x => new INTEGRATION.QuoteModel()
            {
                Id = x.Id,
                OpportunitySysId = x.OpportunitySysId,
                Name = x.Name,
                QuoteName = x.QuoteName,
                AccountMDMID = x.AccountMDMID,
                Type = x.Type != null ? x.Type.Key : string.Empty,
                ContractType = x.ContractType != null ? x.ContractType.Key : string.Empty,
                ExpectedStartDate = x.ExpectedStartDate,
                ExpectedEndDate = x.ExpectedEndDate,
                Term = x.Term,
                FlaggedAs = x.FlaggedAs,
                IsPrimary = x.IsPrimary,
                salestype = x.SalesType != null ? x.SalesType.Key : string.Empty,
                PartnerAccount = x.PartnerAccount != null ? x.PartnerAccount.Id : null,
                PartnershipType = x.PartnershipType != null ? x.PartnershipType.Key : string.Empty,
                ApprovalStage = x.ApprovalStage != null ? x.ApprovalStage.Key : string.Empty,
                ApprovalStatus = x.ApprovalStatus != null ? x.ApprovalStatus.Key : string.Empty,
                NNACV = x.NNACV,
                USDTotalNetNewACV = x.USDTotalNetNewACV,
                ACV = x.ACV,
                USDACV = x.USDACV,
                TCV = x.TCV,
                USDTCV = x.USDTCV,
                RenewalACV = x.RenewalACV,
                USDRenewalACV = x.USDRenewalACV,
                BaseCurrency = x.BaseCurrency,
                PartnerAgreementNumber = x.PartnerAgreementNumber,
                EndCustomerMDMSysID = x.EndCustomerMDMSysID,
                DealRegAssociated = x.DealRegAssociated,
                FieldTerritoryMDMID = x.FieldTerritoryMDMID,
                PSTerritoryMDMID = x.PSTerritoryMDMID,
                QuoteOwner = x.QuoteOwner,
                OpportunityLinkReason = x.OpportunityLinkReason != null ? x.OpportunityLinkReason.Key : string.Empty,
            }).FirstOrDefault();

            //Quote Line Item Detail
            if (objQuote != null && objQuote.IsPrimary == true)
            {
                var quoteLine = quoteData.Where(x => x.QLI != null && !string.IsNullOrEmpty(x.QLI.Id)).Select(x => new INTEGRATION.QuoteLineItem()
                {
                    Id = x.QLI.Id,
                    QuoteId = x.QLI.QuoteId != null ? x.QLI.QuoteId.Id : null,
                    ProductId = x.QLI.OptionId.Id != null ? x.QLI.OptionId.Id : x.QLI.ProductId.Id != null ? x.QLI.ProductId.Id : null,
                    FXRate = x.QLI.FXRate,
                    pricingExchangeRate = x.QLI.FXRate,
                    FXRateDailyUSD = x.QLI.FXRateDailyUSD,
                    ListPrice = x.QLI.ListPrice,
                    PrimaryLineNumber = x.QLI.PrimaryLineNumber,
                    ItemSequenceNumber = x.QLI.ItemSequenceNumber,
                    ParentBundleNumber = x.QLI.ParentBundleNumber,
                    ProductHours = x.QLI.ProductHours,
                    DoNotCalculateNNACV = x.QLI.DoNotCalculateNNACV,
                    USDVariablePriceACVCalc = x.QLI.USDVariablePriceACVCalc,
                    SalesPrice = x.QLI.SalesPrice,
                    Flags = x.FlaggedAs,
                    VariablePriceACVCalc = x.QLI.VariablePriceACVCalc,
                    PriceAdjustment = x.QLI.ListPriceWithAdjustments,
                    RenewalLineACV = x.QLI.RenewalLineACV,
                    StartDate = x.QLI.StartDate,
                    EndDate = x.QLI.EndDate,
                    SellingTerm = x.QLI.SellingTerm,
                    USDSummaryLineACV = x.QLI.USDSummaryLineACV,
                    USDNetNewACV = x.QLI.USDNetNewACV,
                    Quantity = x.QLI.Quantity,
                    //NetNewACV = x.QLI.NetNewACV,
                    NetNewACV = x.QLI.AnnualContractValue,  //Revert after 1.5 sprint
                    ExtendedPrice = x.QLI.ExtendedPrice,
                    USDListPrice = x.QLI.USDListPrice,
                    USDBasePrice = x.QLI.USDBasePrice,
                    USDTotalValue = x.QLI.USDTotalValue,
                    AnnualListPrice = x.QLI.AnnualListPrice,
                    USDSalesPrice = x.QLI.USDSalesPrice,
                    annualListPrice = x.QLI.AnnualListPrice,
                    USDAnnualContractValue = x.QLI.USDAnnualContractValue,
                    AnnualContractValue = x.QLI.AnnualContractValue,
                    AllocationPercent = x.QLI.AllocationPercent,
                    Deployment = x.QLI.Deployment,
                    ResourcePlan = x.QLI.ResourcePlan,
                    TaskID = x.QLI.TaskID,
                    Practice = x.QLI.Practice,
                    LineStatus = x.QLI.LineStatus != null ? x.QLI.LineStatus.Key : string.Empty,
                    LineType = x.QLI.LineType != null ? x.QLI.LineType.Key : string.Empty,
                    Application = x.QLI.Application,
                    RevRecType = x.QLI.ProductId != null && x.QLI.ProductId.DefaultRevRec != null ? x.QLI.ProductId.DefaultRevRec.Key : x.QLI.OptionId.DefaultRevRec != null ? x.QLI.OptionId.DefaultRevRec.Key : null,
                    DeltaQuantity = x.QLI.DeltaQuantity,
                    NetNewQuantity = x.QLI.NetNewQuantity,
					RoleGeneric = x.QLI.ProductId != null & x.QLI.ProductId.RoleGeneric != null ? x.QLI.ProductId.RoleGeneric.Key : x.QLI.OptionId.RoleGeneric != null ? x.QLI.OptionId.RoleGeneric.Key : null,
                }).ToList();

                if (quoteLine != null && quoteLine.Count > 0)
                {
					List<INTEGRATION.QuoteLineItem> objDistinct = quoteLine.GroupBy(x => x.Id).Select(y => y.First()).ToList();
					var resourceGroup = objDistinct.Where(x => !string.IsNullOrEmpty(x.RoleGeneric)).ToList();
					var nonResourceGroup = objDistinct.Where(x => string.IsNullOrEmpty(x.RoleGeneric)).ToList(); ;
					objQuote.QLI = new INTEGRATION.QLIGenericGroup()
					{
						resourceLines = resourceGroup != null && resourceGroup.Count > 0 ? resourceGroup : null,
						lines = nonResourceGroup != null && nonResourceGroup.Count > 0 ? nonResourceGroup : null
					};

					//objQuote.QLI.AddRange(objDistinct);
				}
			}

            //Milestone Detail
            if (objQuote != null && !string.IsNullOrEmpty(quoteData.Select(x => x.MS.Id).FirstOrDefault()))
            {
                var quoteMilestone = quoteData.Select(x => new INTEGRATION.Milestone()
                {
                    Id = x.MS.Id,
                    //Name = x.MS.Name,
                    Amount = x.MS.Amount,
                    CalculationMethod = x.MS.CalculationMethod != null ? x.MS.CalculationMethod.Key : string.Empty,
                    ExtMilestone = x.MS.Milestone,
                    PaymentOrder = x.MS.PaymentOrder,
                    Percent = x.MS.Percent,
                    EstimatedHours = x.MS.ext_EstimatedHours,
                    EstimatedDueDate = x.MS.EstimatedDueDate
                }).ToList();

                if (quoteMilestone != null && quoteMilestone.Count > 0)
                {
                    objQuote.MS = new List<INTEGRATION.Milestone>();
                    List<INTEGRATION.Milestone> objDistinct = quoteMilestone.GroupBy(x => x.Id).Select(y => y.First()).ToList();
                    objQuote.MS.AddRange(objDistinct);
                }
            }
            //Get json
            return JsonConvert.SerializeObject(objQuote);

        }

        /// <summary>
        /// Send Quote Data to mule
        /// </summary>
        /// <param name="quoteData"></param>
        /// <returns></returns>
        public HttpResponseMessage POSTQuoteToMule(List<INTEGRATION.ResponseQuote> quoteData)
        {
            var objQuote = new JavaScriptSerializer().Serialize(quoteData);
            string quoteApiName = ConfigurationManager.AppSettings[Constants.CONFIG_QUOTESYNCAPI];
            return HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, quoteUrl, objQuote, null, quoteApiName));
        }


        /// <summary>
        /// Create Renewal Opportunity to mule
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage CreateRenewalOpportunityToMule(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = accessToken;
            reqConfig.select = Constants.OBJ_RENEWALOPPORTUNITY_SELECTFIELD.Split(Constants.CHAR_COMMA);
            reqConfig.objectName = Constants.OBJ_QUOTE;
            reqConfig.apttusFilterField = Constants.FIELD_ID;
            reqConfig.externalFilterField = Constants.FIELD_ID;
            var searchresponse = Utilities.Search(dictContent, reqConfig);
            if (searchresponse.IsSuccessStatusCode)
            {
                var responseJson = JObject.Parse(searchresponse.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                var responseQuoteModel = JsonConvert.DeserializeObject<List<INTEGRATION.RenewalOpportunityModel>>(responseJson).FirstOrDefault();

                var muleResponse = POSTOpportunityToMule(responseQuoteModel);
                return muleResponse;
            }
            else
            {
                return searchresponse;
            }
        }

        /// <summary>
        /// Post Opportunity to mule
        /// </summary>
        /// <param name="renewalQuoteData"></param>
        /// <returns></returns>
        public HttpResponseMessage POSTOpportunityToMule(INTEGRATION.RenewalOpportunityModel renewalQuoteData)
        {
            INTEGRATION.RenewalOpportunityMuleModel objRenewalOpprtunity = new INTEGRATION.RenewalOpportunityMuleModel()
            {
                mdmID = renewalQuoteData.mdmID,
                type = renewalQuoteData.type != null ? renewalQuoteData.type.Key : null,
                startDate = renewalQuoteData.startDate,
                endDate = renewalQuoteData.endDate,
                term = renewalQuoteData.term != null ? Convert.ToString(renewalQuoteData.term.Value) : "0",
                agreementType = renewalQuoteData.agreementType != null ? renewalQuoteData.agreementType.Key : null,
                baseCurrency = renewalQuoteData.baseCurrency,
                agreementID = renewalQuoteData.agreementID != null ? renewalQuoteData.agreementID.Id : null,
                agreementEndDate = Convert.ToString(renewalQuoteData.agreementEndDate),
                renewalACV = renewalQuoteData.renewalACV != null ? Convert.ToString(renewalQuoteData.renewalACV.Value) : "0.00",
                renewalACVInUSD = renewalQuoteData.renewalACVInUSD != null ? Convert.ToString(renewalQuoteData.renewalACVInUSD.Value) : "0.00"
            };
            var objRenewalQuote = new JavaScriptSerializer().Serialize(objRenewalOpprtunity);
            string renewalOpportunityApiName = ConfigurationManager.AppSettings[Constants.CONFIG_OPPORTUNITYUPSERTAPI];
            return HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, quoteUrl, objRenewalQuote, null, renewalOpportunityApiName));
        }

        /// <summary>
        /// Method Use for upserts quotes on the base of mule response.
        /// </summary>
        /// <param name="muleResponse"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> UpdateRenewalQuoteDetail(List<Dictionary<string, object>> dicContent, string accessToken, HttpResponseMessage muleResponse)
        {
            List<Dictionary<string, object>> objContent = new List<Dictionary<string, object>>();

            var objMule = JsonConvert.DeserializeObject<INTEGRATION.RenewalOpportunityMuleResponseModel>(muleResponse.Content.ReadAsStringAsync().Result);

            string opportunityUpsertQuoteJson = new JavaScriptSerializer().Serialize(objMule);
            var dicOpportunity = JsonConvert.DeserializeObject<Dictionary<string, object>>(opportunityUpsertQuoteJson);

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = accessToken;
            reqConfig.select = new string[] { Constants.FIELD_EXTERNALID, Constants.FIELD_ID };
            reqConfig.objectName = Constants.OBJ_QUOTE;
            reqConfig.apttusFilterField = Constants.FIELD_ID;
            reqConfig.externalFilterField = Constants.FIELD_ID;

            var searchresponse = Utilities.Search(dicContent, reqConfig);
            if (searchresponse.IsSuccessStatusCode)
            {
                var quoteSearch = searchresponse.Content.ReadAsStringAsync().Result;
                var quoteResponseJson = JObject.Parse(quoteSearch).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).FirstOrDefault().ToObject<UpsertResponseModel>();

                dicOpportunity.Add(Constants.FIELD_ID, quoteResponseJson.Id);
                dicOpportunity.Add(Constants.FIELD_EXTERNALID, quoteResponseJson.ExternalId);
            }
            objContent.Add(dicOpportunity);
            return objContent;

        }


        #region PS

        /// <summary>
        /// Create & Populate Generic Resource Related list on Quote/Agreement for SOW 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage GetGenericResourceList(string quoteId, string accessToken)
        {

            try
            {
                var query = new Query(Constants.OBJ_QUOTE);
                query.AddColumns(Constants.OBJ_CARTLINEITEM_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, quoteId));
                query.SetCriteria(exp);

                Join prodcutConfiguration = new Join(Constants.OBJ_QUOTE, Constants.OBJ_PRODUCTCONFIGURATION, Constants.FIELD_ID, Constants.FIELD_QUOTEID, JoinType.INNER, Constants.OBJ_PRODUCTCONFIGURATION);
                Join prodConfigJoin = new Join(Constants.OBJ_PRODUCTCONFIGURATION, Constants.OBJ_LINEITEM, Constants.FIELD_ID, Constants.FIELD_CONFIGURATIONID, JoinType.INNER, Constants.OBJ_LINEITEM);
                Join prodJoin = new Join(Constants.OBJ_LINEITEM, Constants.OBJ_PRODUCT, Constants.FIELD_PRODUCTID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJ_PRODUCT);
                Join prodClassificationJoin = new Join(Constants.OBJ_PRODUCT, Constants.OBJ_PRODUCTCLASSIFICATION, Constants.FIELD_ID, Constants.FIELD_PRODUCTID, JoinType.INNER, Constants.OBJ_PRODUCTCLASSIFICATION);

                query.AddJoin(prodcutConfiguration);
                query.AddJoin(prodConfigJoin);
                query.AddJoin(prodJoin);
                query.AddJoin(prodClassificationJoin);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_QUOTE;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var cartData = JsonConvert.DeserializeObject<List<GenericResourceModel>>(responseString);


                    var filterResults = from c in cartData
                                        where c.ProductClassification.ClassificationId.Name == Constants.STR_TAILOREDSERVICES && c.LineItem.OptionId.RoleGeneric != null
                                        group c by c.LineItem.OptionId.RoleGeneric.Value into grps
                                        select new
                                        {
                                            QuoteId = grps.Select(a => a.Id).FirstOrDefault(),
                                            AgreementId = grps.Select(a => a.MasterAgreementId).FirstOrDefault(),
                                            GenericResource = grps.Key,
                                            Quantity = grps.Sum(a => a.LineItem.Quantity),
                                            BasePrice = grps.Average(a => a.LineItem.BasePrice),
                                            NetPrice = grps.Sum(a => a.LineItem.NetPrice)
                                        };


                    if (filterResults != null)
                    {
                        var resourceConfig = new RequestConfigModel();
                        resourceConfig.accessToken = accessToken;
                        resourceConfig.objectName = Constants.OBJ_PSSOWRESOURCE;
                        var resourceList = new List<dynamic>();

                        foreach (var filterResult in filterResults)
                        {
                            resourceList.Add(new
                            {
                                ext_QuoteId = filterResult.QuoteId,
                                ext_AgreementId = filterResult.AgreementId,
                                ext_GenericResource = filterResult.GenericResource,
                                ext_Quantity = filterResult.Quantity,
                                ext_BasePrice = filterResult.BasePrice,
                                ext_NetPrice = filterResult.NetPrice
                            });
                        }
                        if (resourceList != null)
                        {
                            var responseResult = Utilities.Create(resourceList, resourceConfig);
                        }
                    }
                    var serializedObj = JsonConvert.SerializeObject(filterResults);
                    var deserializedObj = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(serializedObj.ToString());
                    var contentSerialized = JsonConvert.SerializeObject(deserializedObj);


                    var resp = new HttpResponseMessage
                    {
                        Content = new StringContent(contentSerialized, System.Text.Encoding.UTF8, "application/json"),
                        StatusCode = System.Net.HttpStatusCode.OK
                    };
                    return resp;
                }


                return Utilities.CreateResponse(response);
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
        }
        #endregion PS
    }
}
