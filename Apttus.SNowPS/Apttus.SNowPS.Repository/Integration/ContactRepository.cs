﻿/****************************************************************************************
@Name: ContactRepository.cs
@Author: Varun Shah
@CreateDate: 5 Oct 2017
@Description: Contact Related Computations
@UsedBy: This will be used by ContactController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Respository
{
    public class ContactRepository
    {
        /// <summary>
        /// Contact Advance Search and Post
        /// </summary>
        /// <returns></returns>
        #region Properties
        private string contactUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        #endregion

        #region Methods
        /// <summary>
        /// Resolves external ids and upserts contacts using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertContact(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var errors = new List<ErrorInfo>();
                var responseContact = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                //Resolve Account MDM Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_EXT_PARENTCUSTOMERMDMID;//"MDM ID";
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;//"ExternalId";
                reqConfig.resolvedID = Constants.FIELD_PARENTCUSTOMERID;//Apttus Lookup Field

                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Upsert Contact
                    reqConfig.objectName = Constants.OBJ_CONTACT;
                    reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                    responseContact = Utilities.Upsert(dictContent, reqConfig);

                    return Utilities.CreateResponse(responseContact, errors);
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseContact.Content = Utilities.CreateJsonContent(jsonResponse);

                    //Create Response by adding custom errors
                    return Utilities.CreateResponse(responseContact, errors);
                }
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Get Apttus Contact Data By ContactId
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage GetApttusDataByContactId(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_CONTACT, Constants.FIELD_ID, Constants.FIELD_ID);

                return Utilities.Search(dictContent, requestConfig);
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// PUT Apttus Contact Data To Mule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage PUTContactToMule(ContactModel model)
        {
            try
            {
                var objContact = new JavaScriptSerializer().Serialize(model);
                return HttpActions.PutRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, contactUrl, objContact, null, string.Format(ConfigurationManager.AppSettings[Constants.CONFIG_CONTACTUPDATEAPI], model.consumerID)));
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// POST Apttus Contact Data To Mule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage POSTContactToMule(ContactModel model)
        {
            try
            {
                var objContact = new JavaScriptSerializer().Serialize(model);
                return HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, contactUrl, objContact, null, ConfigurationManager.AppSettings[Constants.CONFIG_CONTACTCREATEAPI]));
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Update Apttus Contact Data 
        /// </summary>
        /// <param name="dictionaryContact"></param>
        /// <returns></returns>
        public HttpResponseMessage UpdateContactToApttus(Dictionary<string, string> dictionaryContact)
        {
            try
            {
                var resUpsert = Utilities.UpdateMuleResponse(dictionaryContact[Constants.FIELD_CONSUMERID.ToString()], dictionaryContact[Constants.FIELD_MULERESPONSEID], Constants.OBJ_CONTACT);
                return Utilities.CreateResponse(resUpsert);
            }
            catch
            {
                //TODO
                throw;
            }
        }

        public HttpResponseMessage SendContactDataToMule(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                string objResponse = string.Empty;
                string resContactString = string.Empty;

                //GetApttusContactData
                var resMessageContact = GetApttusDataByContactId(dictContent);

                if (resMessageContact.IsSuccessStatusCode)
                {
                    resContactString = resMessageContact.Content.ReadAsStringAsync().Result;
                    var responseContact = JObject.Parse(resContactString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<ContactModel>>();
                    HttpResponseMessage contactResponse = new HttpResponseMessage();

                    if (responseContact != null && responseContact.Count > 0)
                    {
                        //SendContactDataToMule
                        objResponse = new JavaScriptSerializer().Serialize(responseContact);
                        
                        if (string.IsNullOrEmpty(responseContact.First().consumerID))
                        {
                            contactResponse = POSTContactToMule(responseContact[0]);
                            if (contactResponse.IsSuccessStatusCode)
                            {
                                //UpsertContactData
                                var resContactUpsert = contactResponse.Content.ReadAsStringAsync().Result;
                                var res = JObject.Parse(resContactUpsert).ToObject<Dictionary<string, string>>();

                                if (res != null && res.Count > 0 && res.ContainsKey(Constants.FIELD_MULERESPONSEID) && res.ContainsKey(Constants.FIELD_CONSUMERID))
                                    return UpdateContactToApttus(res);
                                else
                                    return contactResponse;
                            }
                            return contactResponse;
                        }
                        else
                        {
                            var message = new Dictionary<string, object>() { { "Message", "Contact is already synched."} };
                            return Utilities.CreateResponse(message, HttpStatusCode.BadRequest);
                        }
                    }
                    else
                    {
                        var message = new Dictionary<string, object>() { { "Message", "Contact details not found." } };
                        return Utilities.CreateResponse(message, HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    return resMessageContact;
                }
            }
            catch
            {
                //TODO
                throw;
            }

        }
        #endregion
    }
}
