﻿/*************************************************************
@Name: AgreementRepository.cs
@Author: Bharat Kumbhar   
@CreateDate: 08-Nov-2017
@Description: This class contains the implementation for Approval Request repository
@UsedBy:  
******************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription:
******************************************************************/

using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Apttus.SNowPS.Repository.CLM
{
    public class ApprovalRequestRepository
    {
        public string AccessToken { get; set; }

        public ApprovalRequestRepository(string accessToken)
        {
            AccessToken = accessToken;
        }
        /// <summary>
        /// Copies the agreement identifier from the object field of Approval request.
        /// </summary>
        /// <param name="approvalRequestId">The approval request identifier.</param>
        /// <returns></returns>
        public void CopyAgreementId(Guid approvalRequestId)
        {
            try
            {
                /*Query expression*/
                var approvalRequestQuery = new Query
                {
                    EntityName = ApprovalRequestConstants.ENTITYNAME,
                    Columns = ApprovalRequestConstants.FIELDS_COPYAGREEMENTID.Split(Constants.CHAR_COMMA).ToList(),
                    Criteria = new Expression
                    {
                        Conditions = new List<Condition>
                        {
                            new Condition /*Filter with approval request id.*/
                            {
                                FieldName      = Constants.FIELD_ID,
                                FilterOperator = FilterOperator.Equal,
                                Value          = approvalRequestId
                            }
                        }
                    }
                };

                var response = Utilities.Search(approvalRequestQuery.Serialize(), new RequestConfigModel
                {
                    accessToken = AccessToken,
                    searchType = SearchType.AQL,
                    objectName = ApprovalRequestConstants.ENTITYNAME
                });

                if (response?.Content != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                    var approvalRequests = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);
                    
                    if (approvalRequests != null && approvalRequests.Count > 0)
                    {
                        Guid? agreementId = null;

                        if(approvalRequests[0].ContainsKey(ApprovalRequestConstants.OBJECT) && approvalRequests[0][ApprovalRequestConstants.OBJECT] != null)
                        {
                            var objectId = JObject.Parse(approvalRequests[0][ApprovalRequestConstants.OBJECT].ConvertToString()).ToObject <Dictionary<string, object>>();

                            if (objectId?["Type"]?.ToString() == AgreementObjectConstants.ENTITYNAME)
                            {
                                agreementId = new Guid(objectId[Constants.FIELD_ID].ToString());
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            return;
                        }

                        var agreements = new List<dynamic>();
                        agreements.Add(new
                        {
                            Id = approvalRequestId,
                            ext_AgreementId = new
                            {
                                Id = agreementId.Value
                            }
                        });
                        var updateResponse = Utilities.Update(agreements, new RequestConfigModel
                        {
                            objectName = ApprovalRequestConstants.ENTITYNAME,
                            resolvedID = approvalRequestId.ToString(),
                            accessToken = AccessToken
                        });
                    }

                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                {
                    throw exc.InnerException;
                }
                throw exc;
            }
        }
    }
}
