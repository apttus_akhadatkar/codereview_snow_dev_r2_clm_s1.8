﻿/****************************************************************************************
@Name: ParticipantRepository.cs
@Author: Bhavinkumar Mistry
@CreateDate: 26 Oct 2017
@Description: Agreement Participant related business logic
@UsedBy: This will be used by AgreementController.cs and QuoteController.cs
*****************************************************************************************/


using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Repository.CLM
{
    /// <summary>
    /// Participant Repository
    /// </summary>
    public sealed class ParticipantRepository
    {
        #region Private Fields

        /// <summary>
        /// Static Object of Participant Repository
        /// </summary>
        private static readonly ParticipantRepository participantRepository = new ParticipantRepository();

        #endregion

        #region Public Properies

        /// <summary>
        /// Property to store Authentication Token
        /// </summary>
        public string AccessToken { get; set; }

        #endregion

        #region Static Constructor

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static ParticipantRepository()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Use to Get Single Instance of AgreementClauseRepository
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public static ParticipantRepository Instance(string authToken)
        {
            if (participantRepository != null)
            {
                participantRepository.AccessToken = authToken;
            }
            return participantRepository;
        }

        /// <summary>
        /// Upserts the entry of Participant for the related agreement based on the Quote Participant
        /// </summary>
        /// <param name="participant">Participants</param>
        /// <returns></returns>
        public HttpResponseMessage UpsertParticipantForAgreement(Dictionary<string, object> participant)
        {
            var responseParticipant = new HttpResponseMessage();
            var participantRoles = GetParticipantRoles(participant);
            var contextObject =
                JObject.Parse(participant[ParticipantConstants.CONTEXTOBJECT].ToString()).ToObject<ContextObject>();
            var agreements =
                AgreementRepository.Instance(AccessToken)
                    .GetQuoteRelatedAgreements(new Dictionary<string, object> { { Constants.FIELD_ID, contextObject.Id } },
                        new List<string> { Constants.FIELD_ID, AgreementObjectConstants.EXT_QUOTEID });

            if (agreements != null && agreements.Count > 0)
            {
                //Updating Participant Records with the new ContextObject
                Parallel.ForEach(agreements, agreement =>
                {
                    participant[ParticipantConstants.CONTEXTOBJECT] = new ContextObject
                    {
                        Id = agreement[Constants.FIELD_ID].ToString(),
                        Type = AgreementObjectConstants.ENTITYNAME
                    };
                    participant.Remove(Constants.FIELD_ID);
                    UpsertParticipant(participant, participantRoles);
                });
            }

            if (participant != null && participant.Count > 0)
            {
                responseParticipant = UpsertParticipant(participant, participantRoles);
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                responseParticipant.Content = Utilities.CreateJsonContent(jsonResponse);
                Utilities.CreateResponse(responseParticipant);
            }
            return responseParticipant;
        }

        /// <summary>
        /// Get Participant Roles for the Participant
        /// </summary>
        /// <param name="participant"></param>
        /// <returns></returns>
        private List<Dictionary<string, object>> GetParticipantRoles(Dictionary<string, object> participant)
        {
            var query = new Query
            {
                EntityName = ParticipantRoleConstants.ENTITYNAME,
                Columns =
                    new List<string>
                    {
                        ParticipantRoleConstants.NAME,
                        ParticipantRoleConstants.ROLE,
                        ParticipantRoleConstants.PARTICIPANTID,
                        ParticipantRoleConstants.EXTERNALID
                    },
                Criteria = new Expression
                {
                    Conditions =
                    {
                        new Condition
                        {
                            FieldName = ParticipantRoleConstants.PARTICIPANTID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value = participant[Constants.FIELD_ID]
                        }
                    }
                }
            };
            var responseMessage = Utilities.Search(query.Serialize(),
                new RequestConfigModel { accessToken = AccessToken, objectName = ParticipantConstants.ENTITYNAME });
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;
            var participantRoles = new List<Dictionary<string, object>>();
            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {
                participantRoles =
                    JObject.Parse(responseString)
                        .SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)
                        .ToObject<List<Dictionary<string, object>>>();
            }
            return participantRoles;
        }

        /// <summary>
        /// Upsert Participant Roles
        /// </summary>
        /// <param name="participantRoles"></param>
        /// <returns></returns>
        private HttpResponseMessage UpsertParticipantRoles(List<Dictionary<string, object>> participantRoles)
        {
            var responseParticipantRoles = new HttpResponseMessage();
            //Convert content to case-insensitive
            participantRoles = Utilities.GetCaseIgnoreDictContent(participantRoles);
            if (participantRoles != null && participantRoles.Count > 0)
            {
                //Upsert Participant Role
                responseParticipantRoles = Utilities.Upsert(participantRoles,
                    new RequestConfigModel
                    {
                        objectName = ParticipantRoleConstants.ENTITYNAME,
                        UpsertKey = Constants.FIELD_ID,
                        accessToken = AccessToken
                    });
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                responseParticipantRoles.Content = Utilities.CreateJsonContent(jsonResponse);
            }
            return responseParticipantRoles;
        }

        /// <summary>
        /// Copy Related Participants from Quote to Agreement
        /// </summary>
        /// <param name="agreement">Agreement Model</param>
        /// <param name="fieldsToTransfer"></param>
        /// <returns></returns>
        public HttpResponseMessage CopyRelatedParticipantsFromQuote(Dictionary<string, object> agreement,
            List<string> fieldsToTransfer)
        {
            var resultResponse = new HttpResponseMessage();
            //Get Related Participants based on Quote
            var quoteParticipants = GetParticipantsBasedonContext(QuoteConstants.ENTITYNAME,
                agreement[AgreementObjectConstants.EXT_QUOTEID].ToString(), fieldsToTransfer);
            if (quoteParticipants == null || quoteParticipants.Count == 0)
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                resultResponse.Content = Utilities.CreateJsonContent(jsonResponse);
                Utilities.CreateResponse(resultResponse);
            }
            else
            {
                Parallel.ForEach(quoteParticipants, participant =>
                {
                    var participantRoles = GetParticipantRoles(participant);
                    ContextObject contextObject =
                        JObject.Parse(participant[ParticipantConstants.CONTEXTOBJECT].ToString())
                            .ToObject<ContextObject>();
                    if (contextObject.Type == QuoteConstants.ENTITYNAME &&
                        contextObject.Id == agreement[Constants.FIELD_EXT_QUOTEID].ToString())
                    {
                        participant[ParticipantConstants.CONTEXTOBJECT] = new ContextObject
                        {
                            Id = agreement[Constants.FIELD_ID].ToString(),
                            Type = AgreementObjectConstants.ENTITYNAME
                        };
                        participant.Remove(Constants.FIELD_ID);
                    }
                    var response = UpsertParticipant(participant, participantRoles);
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        resultResponse = Utilities.CreateResponse(resultResponse, response);
                    }
                });
            }
            return resultResponse;
        }

        /// <summary>
        /// Get the participants based on the ContextObject and ContextObject Id
        /// </summary>
        /// <param name="contextObject"></param>
        /// <param name="id"></param>
        /// <param name="selectFields"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetParticipantsBasedonContext(string contextObject, string id,
            List<string> selectFields)
        {
            //Creating a Query
            var query = new Query
            {
                EntityName = ParticipantConstants.ENTITYNAME,
                Columns = selectFields,
                Criteria = new Expression
                {
                    Conditions =
                    {
                        new Condition
                        {
                            FieldName = ParticipantConstants.CONTEXTOBJECT,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value = new ContextObject
                            {
                                Id = id,
                                Type = contextObject
                            }
                        }
                    }
                }
            };
            var responseMessage = Utilities.Search(query.Serialize(),
                new RequestConfigModel { accessToken = AccessToken, objectName = ParticipantConstants.ENTITYNAME });
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;
            var participants = new List<Dictionary<string, object>>();
            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {
                participants =
                    JObject.Parse(responseString)
                        .SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)
                        .ToObject<List<Dictionary<string, object>>>();
            }
            return participants;
        }

        /// <summary>
        /// Upsert the participants
        /// </summary>
        /// <param name="participant"></param>
        /// <param name="participantRoles">ParticipantRoles to be updated/inserted</param>
        /// <returns>List of Participants</returns>
        public HttpResponseMessage UpsertParticipant(Dictionary<string, object> participant,
            List<Dictionary<string, object>> participantRoles)
        {
            var response = Utilities.Upsert(new List<Dictionary<string, object>> { participant },
                new RequestConfigModel
                {
                    objectName = ParticipantConstants.ENTITYNAME,
                    UpsertKey = Constants.FIELD_ID,
                    accessToken = AccessToken
                });
            if (response != null && response.IsSuccessStatusCode)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var responseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseString);
                if (responseObj != null && (responseObj.Inserted.Count > 0 || responseObj.Updated.Count > 0))
                {
                    var upsertedParticipant = new Dictionary<string, object>();
                    if (responseObj.Inserted.Count > 0)
                    {
                        upsertedParticipant = responseObj.Inserted[0];
                    }
                    else if (responseObj.Updated.Count > 0)
                    {
                        upsertedParticipant = responseObj.Updated[0];
                    }

                    if (upsertedParticipant.ContainsKey(Constants.FIELD_ID))
                    {
                        Parallel.ForEach(participantRoles, applicableParticipantRole =>
                        {
                            applicableParticipantRole[ParticipantRoleConstants.PARTICIPANTID] =
                                upsertedParticipant[Constants.FIELD_ID];
                            applicableParticipantRole.Remove(Constants.FIELD_ID);
                        });
                    }
                }
                var participantRoleResponse = UpsertParticipantRoles(participantRoles);
                response = Utilities.MergeErrorResponse(response, participantRoleResponse);
            }
            return response;
        }


        /// <summary>
        /// Delete the participants
        /// </summary>
        /// <param name="participants">Participants to be deleted</param>
        /// <returns>List of Participants</returns>
        public HttpResponseMessage DeleteParticipants(List<Dictionary<string, object>> participants)
        {
            HttpResponseMessage deleteResponse = new HttpResponseMessage();
            ConcurrentQueue<string> participantIds = new ConcurrentQueue<string>();
            Parallel.ForEach(participants,
                (participant) => participantIds.Enqueue(participant[Constants.FIELD_ID].ToString()));

            //Find the Participant Role entries for these participants
            var query = new Query
            {
                EntityName = ParticipantConstants.ENTITYNAME,
                Columns = ParticipantRoleConstants.FIELDS.Split(Constants.CHAR_COMMA).ToList(),
                Criteria = new Expression
                {
                    Conditions =
                    {
                        new Condition
                        {
                            FieldName = ParticipantRoleConstants.PARTICIPANTID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.In,
                            Value = participantIds
                        }
                    }
                }
            };
            var responseMessage = Utilities.Search(query.Serialize(),
                new RequestConfigModel { accessToken = AccessToken, objectName = ParticipantRoleConstants.ENTITYNAME });
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;
            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {
                var participantRoles =
                    JObject.Parse(responseString)
                        .SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)
                        .ToObject<List<Dictionary<string, object>>>();
                var participantRoleDeleteResponse = new HttpResponseMessage();
                if (participantRoles != null && participantRoles.Count > 0)
                {
                    participantRoleDeleteResponse = Utilities.DeleteMultiple(participantRoles,
                        new RequestConfigModel
                        {
                            objectName = ParticipantRoleConstants.ENTITYNAME,
                            accessToken = AccessToken
                        });
                }
                var participantDeleteResponse = Utilities.DeleteMultiple(participants,
                new RequestConfigModel
                {
                    objectName = ParticipantConstants.ENTITYNAME,
                    accessToken = AccessToken
                });

                deleteResponse = Utilities.CreateResponse(participantDeleteResponse, participantRoleDeleteResponse);
            }
            return deleteResponse;
        }

        /// <summary>
        /// Delete Participant from the related Agreement
        /// </summary>
        /// <param name="participant"></param>
        /// <returns></returns>
        public HttpResponseMessage DeleteParticipantForAgreement(Dictionary<string, object> participant)
        {
            //Get the participant which same External Id
            var resultResponse = new HttpResponseMessage();
            var query = new Query
            {
                EntityName = ParticipantConstants.ENTITYNAME,
                Columns = new List<string> { Constants.FIELD_ID },
                Criteria = new Expression
                {
                    Conditions =
                    {
                        new Condition
                        {
                            FieldName = Constants.FIELD_ID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value = participant[Constants.FIELD_ID]
                        }
                    }
                }
            };
            var participants = new List<Dictionary<string, object>>();
            var responseMessage = Utilities.Search(query.Serialize(),
                new RequestConfigModel { accessToken = AccessToken, objectName = ParticipantConstants.ENTITYNAME });
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;
            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {

                participants =
                    JObject.Parse(responseString)
                        .SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)
                        .ToObject<List<Dictionary<string, object>>>();
            }
            if (participants != null && participants.Count > 0)
            {
                resultResponse = DeleteParticipants(participants);
            }
            return resultResponse;
        }

        #endregion
    }
}
