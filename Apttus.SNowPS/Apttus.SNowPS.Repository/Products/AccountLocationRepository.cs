﻿/****************************************************************************************
@Name: AccountLocationRepository.cs
@Author: Maunish P. Shah
@CreateDate: 22 Nov 2017
@Description: Account Location Related Computations
@UsedBy: This will be used by AccountLocationController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Repository.Products
{
    public class AccountLocationRepository
    {
        #region Private Fields
        /// <summary>
        /// Static object of AccountLocationRepository
        /// </summary>
        private static AccountLocationRepository accountLocationRepository;
        #endregion

        #region Public Properties
        /// <summary>
        /// Property to store Authentication token
        /// </summary>
        public string AuthToken { get; set; }
        #endregion

        #region Constructors
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static AccountLocationRepository()
        {
        }

        /// <summary>
        /// Parameterized constuctor
        /// </summary>
        /// <param name="authToken"></param>
        private AccountLocationRepository(string authToken)
        {
            AuthToken = authToken;
        }
        #endregion

        /// <summary>
        /// Use to Get Single Instance of AccountLocationRepository
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public static AccountLocationRepository Instance(string authToken)
        {
            if (accountLocationRepository == null)
            {
                accountLocationRepository = new AccountLocationRepository(authToken);
            }
            else
            {
                accountLocationRepository.AuthToken = authToken;
            }
            return accountLocationRepository;
        }

        /// <summary>
        /// Method to retrive AccountLocationRepository based on the accountId
        /// </summary>
        /// <param name="accountId">Input accountId</param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage GetAccountDetailsById(string accountId)
        {
            try
            {
                var query = new Query(Constants.OBJ_ACCOUNT);
                query.AddColumns(AccountLocationConstants.OBJ_ACCOUNTLOCATIONS_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, accountId));
                query.SetCriteria(exp);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = AuthToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var accountInfo = JsonConvert.DeserializeObject<List<AccountLocationsModel>>(responseString);

                    var serializedObj = JsonConvert.SerializeObject(accountInfo);
                    var deserializedObj = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(serializedObj.ToString());
                    var contentSerialized = JsonConvert.SerializeObject(deserializedObj);


                    var resp = new HttpResponseMessage
                    {
                        Content = new StringContent(contentSerialized, System.Text.Encoding.UTF8, "application/json"),
                        StatusCode = System.Net.HttpStatusCode.OK
                    };
                    return resp;
                }

                return Utilities.CreateResponse(response);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
        }


    }
}
