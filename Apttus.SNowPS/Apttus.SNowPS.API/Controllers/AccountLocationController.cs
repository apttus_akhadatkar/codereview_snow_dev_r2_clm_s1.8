﻿/****************************************************************************************
@Name: AccountLocationController.cs
@Author: Maunish P. Shah
@CreateDate: 22 Nov 2017
@Description: API calls related to Account Location Entity

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription:
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository.Products;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    /// <summary>
    /// AccountLocation Contoller
    /// </summary>
    [ServiceRequestActionFilter]
    public class AccountLocationController : BaseController
    {
        #region Action Methods

        /// <summary>
        /// Action Method to get Account data based on ID. The main column needed is the external ID
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>HttpResponseMessage</returns>        
        [ActionName("Account")]
        [HttpPost]
        public HttpResponseMessage GetAccountDetailsById([FromBody]string accountId)
        {
            try
            {
                if (Request != null && Request.RequestUri != null)
                {
                    var accountLocationRepository = AccountLocationRepository.Instance(AccessToken);
                    var response = accountLocationRepository.GetAccountDetailsById(accountId);
                    if (response != null)
                        return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(response.Content.ReadAsStringAsync().Result));
                    else
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        #endregion
    }
}
