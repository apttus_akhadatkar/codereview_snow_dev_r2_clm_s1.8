﻿/****************************************************************************************
@Name: ParticipantController.cs
@Author: Mahesh Patel
@CreateDate: 13 Nov 2017
@Description: Participant Related Logic
@UsedBy: This will be used as an API calls

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository.CLM;
using Apttus.SNowPS.Repository.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLMParticipantRepository = Apttus.SNowPS.Repository.CLM.ParticipantRepository;

namespace Apttus.SNowPS.API.Controllers
{
    public class ParticipantController: BaseController
    {
        private readonly ParticipantsRepository _participant = new ParticipantsRepository();

        /// <summary>
        /// Upsert Stackholders in Participant Object
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertStackholders([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        return ParticipantsRepository.Instance(accessToken).UpsertParticipant(dictContent);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Upsert Participant's entry for related Agreement
        /// </summary>
        /// <param name="participant"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpsertParticipantForAgreement([FromBody]Dictionary<string, object> participant)
        {
            try
            {
                if (participant != null)
                {
                    return Ok(ParticipantRepository.Instance(AccessToken).CopyRelatedParticipantsFromQuote(participant, new List<string> { ParticipantConstants.CONTEXTOBJECT, ParticipantConstants.EXTERNALID, ParticipantConstants.EXT_ISACTIVE, ParticipantConstants.EXT_JOBID, ParticipantConstants.EXT_JOBNAME, ParticipantConstants.PARTICIPANT }));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Delete Participant Entry for Related Agreement
        /// </summary>
        /// <param name="participant"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult DeleteParticipantForAgreement([FromBody]Dictionary<string, object> participant)
        {
            try
            {
                if (participant != null)
                {
                    return Ok(ParticipantRepository.Instance(AccessToken).DeleteParticipantForAgreement(participant));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
