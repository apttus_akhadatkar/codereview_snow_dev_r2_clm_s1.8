﻿/****************************************************************************************
@Name: UserController.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Resolves ExternalIDs and Upserts users and roles using generic API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class UserController : ApiController
    {
        private readonly UserRepository _territories = new UserRepository();

        /// <summary>
        /// User Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                string jsonResponse = string.Empty;
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_USER;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Resolves external ids and upserts users and roles using standard API. Assigns default role to user at the time of creation if role is not provided
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertUser([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                List<ErrorInfo> errors = new List<ErrorInfo>();

                if (dictContent != null && dictContent.Count() > 0)
                {
                    //Get Access Token from Headers
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        //Upsert User
                        return _territories.UpsertUsers(dictContent, accessToken);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        #region PS
        /// <summary>
        /// When a user on stakeholders is updated, remove the user role assigned to the user, if the user does not exist on the stakeholder list for any other quote with the same reference
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("RemoveUserRoleOnUpdateStakeholder")]
        [HttpPost]
        public HttpResponseMessage RemoveUserRoleOnUpdateStakeholder([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var result = _territories.RemoveUserRoleOnUpdateStakeholder(dictContent, accessToken);
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(result.Content.ReadAsStringAsync().Result));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User Id and Role Id is required");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Add User Role
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("AddUserRoleOnUpdateStakeholder")]
        [HttpPost]
        public HttpResponseMessage AddUserRoleOnUpdateStakeholder([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var result = _territories.AddUserRoleOnUpdateStakeholder(dictContent, accessToken);
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(result.Content.ReadAsStringAsync().Result));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User Id and Role Id is required");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }


        /// <summary>
        /// Remove User Role on update User Group Membership
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("RemoveUserRoleOnUpdateUserGroupMembership")]
        [HttpPost]
        public HttpResponseMessage RemoveUserRoleOnUpdateUserGroupMembership([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var dicUserGroupMem = Utilities.GetCaseIgnoreDictContent(dictContent);
                    string Id = Convert.ToString(dicUserGroupMem.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                    var result = _territories.RemoveUserRoleOnUpdateUserGroupMembership(Id, accessToken);

                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(result.Content.ReadAsStringAsync().Result));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User Gorup Member Id is required");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Add User Role on update User Group Membership
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("AddUserRoleOnUpdateUserGroupMembership")]
        [HttpPost]
        public HttpResponseMessage AddUserRoleOnUpdateUserGroupMembership([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var dicUserGroupMem = Utilities.GetCaseIgnoreDictContent(dictContent);
                    string Id = Convert.ToString(dicUserGroupMem.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                    var result = _territories.AddUserRoleOnUpdateUserGroupMembership(Id, accessToken);

                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(result.Content.ReadAsStringAsync().Result));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User Gorup Member Id is required");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }


        #endregion PS
    }
}
