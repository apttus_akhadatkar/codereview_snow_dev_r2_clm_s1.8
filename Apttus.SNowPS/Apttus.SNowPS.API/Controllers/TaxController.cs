﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Respository;

namespace Apttus.SNowPS.API.Controllers
{
    public class TaxController : ApiController
    {
        // GET: api/Tax/5
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage respMsg = new HttpResponseMessage();
            try
            {                
                var respContacts = string.Empty;
                var headers = this.Request.Headers;
                var accessToken = headers.Contains("Authorization") ? headers.GetValues("Authorization").First() : null;

                //Generate authentication token
                if (accessToken == null)
                    accessToken = Utilities.GetAuthToken();

                QuoteModel quote = TaxRepository.GetQuote(id, accessToken);

                if (quote != null)
                {
                    if (!quote.TaxExempt)
                    {
                        AccountLocationModel shipToAccountLocation = TaxRepository.GetShipToAccountLocation(quote.ShipToLocation, accessToken);
                        string sellingEntity = quote.SellingEntity;
                        string country = String.Empty;
                        string state = String.Empty;
                        if (shipToAccountLocation != null)
                        {
                            country = shipToAccountLocation.Country;
                            state = shipToAccountLocation.State;
                        }

                        if (!String.IsNullOrEmpty(sellingEntity) && !String.IsNullOrEmpty(country) && !String.IsNullOrEmpty(state))
                        {
                            List<QuoteLineItem> qLIems = TaxRepository.GetQuoteLineItems(id, accessToken);
                            decimal subTax = 0;
                            decimal eduTax = 0;
                            decimal serTax = 0;

                            if (qLIems != null && qLIems.Count > 0)
                            {
                                List<Dictionary<string, object>> listQLITaxes = new List<Dictionary<string, object>>();
                                qLIems.ForEach(qLI =>
                                {
                                    decimal taxRate = 0;
                                    Dictionary<string, object> qLTax = new Dictionary<string, object>();

                                    Product product = TaxRepository.GetProductFamily(qLI.ProductId, accessToken);
                                    string contributesTo = product.ExtFamily != null ? product.ExtFamily.Value : null;
                                    //string contributesTo = qLI.ContributesTo;
                                    if (!String.IsNullOrEmpty(contributesTo)
                                            && !String.IsNullOrEmpty(sellingEntity)
                                            && !String.IsNullOrEmpty(country)
                                            && !String.IsNullOrEmpty(state))
                                    {
                                        taxRate = TaxRepository.GetTaxRate(contributesTo, sellingEntity, country, state, Constants.MISC_SALES_TAX, accessToken);

                                        //decimal lineAmount = qLI.TCV != null ? qLI.TCV.Value : 0;
                                        decimal lineAmount = qLI.NetPrice != null ? qLI.NetPrice.Value : 0;
                                        decimal tax = (lineAmount * (taxRate / 100));
                                        qLTax.Add(Constants.FIELD_ID, qLI.Id);
                                        qLTax.Add(Constants.FIELD_EXT_TAX, tax.ToString());
                                        listQLITaxes.Add(qLTax);

                                        if (contributesTo != null)
                                        {
                                            if (contributesTo.Contains(Constants.SUBSCRIPTION))
                                                subTax += tax;
                                            else if (contributesTo.Contains(Constants.EDUCATION))
                                                eduTax += tax;
                                            else if (contributesTo.Contains(Constants.SERVICES))
                                                serTax += tax;
                                        }

                                        //if (contributesTo != null)
                                        //{
                                        //    if (contributesTo.ToLower() == Constants.SUBSCRIPTION_LICENSE_REVENUE.ToLower() || contributesTo.ToLower() == Constants.LICENSE_REVENUE.ToLower())
                                        //        subTax += tax;
                                        //    else if (contributesTo.ToLower() == Constants.TRAINING_REVENUE.ToLower())
                                        //        eduTax += tax;
                                        //    else if (contributesTo.ToLower() == Constants.TOTAL_PS_REVENUE.ToLower())
                                        //         serTax += tax;
                                        //}

                                        //else subTax += tax;
                                    }
                                }

                                );

                                //update lineitems
                                //UpdateRequestModel<object> updReqModel = new UpdateRequestModel<object>();
                                //updReqModel.requestModel = listQLITaxes.ToArray();
                                // var jsonRequestuery = new JavaScriptSerializer().Serialize(updReqModel.requestModel);
                                var reqConfig = new RequestConfigModel();
                                reqConfig.accessToken = accessToken;
                                reqConfig.objectName = Constants.OBJ_CPQ_QUOTELINEITEM;
                                respMsg = Utilities.Upsert(listQLITaxes, reqConfig, false);

                                //update quote
                                SelectOption taxCalculation = new SelectOption();
                                taxCalculation.Key = Constants.MISC_COMPLETED;
                                taxCalculation.Value = Constants.MISC_COMPLETED;

                                Dictionary<string, object> quoteLITaxes = new Dictionary<string, object>();
                                quoteLITaxes.Add(Constants.FIELD_ID, id);
                                quoteLITaxes.Add(Constants.FIELD_EXT_TAXPS, serTax.ToString());
                                quoteLITaxes.Add(Constants.FIELD_EXT_TAXEDUCATION, eduTax.ToString());
                                quoteLITaxes.Add(Constants.FIELD_EXT_TAXSUBSCRIPTION, subTax.ToString());
                                quoteLITaxes.Add(Constants.FIELD_EXT_TAXCALCULATION, taxCalculation.Key);

                                listQLITaxes.Clear();
                                listQLITaxes.Add(quoteLITaxes);

                                //updReqModel.requestModel = lstQLTaxes;
                                //jsonRequestuery = new JavaScriptSerializer().Serialize(updReqModel.requestModel);
                                reqConfig.objectName = Constants.OBJ_QUOTE;
                                Utilities.Upsert(listQLITaxes, reqConfig, false);
                            }
                        }

                        else
                        {
                            respMsg.Content = new StringContent(Constants.ERR_TAX_MANDATORYFIELDSNULL);
                        }
                    }

                    else
                    {
                        respMsg.Content = new StringContent(Constants.ERR_TAX_EXEMPTTRUE);
                    }
                }

                else
                {
                    respMsg.Content = new StringContent(Constants.ERR_TAX_NOQUOTEFOUND);
                }
                return respMsg;
            }
            catch (Exception ex)
            {
                respMsg.Content = new StringContent(ex.Message);
                respMsg.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(respMsg);
            }
        }

        // POST: api/Tax
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Tax/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Tax/5
        public void Delete(int id)
        {
        }
    }
}
