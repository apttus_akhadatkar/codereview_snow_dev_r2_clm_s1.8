﻿/****************************************************************************************
@Name: AgreementLineItemController.cs
@Author: Bharat Kumbhar
@CreateDate: 15 Nov 2017
@Description: for AgreementLineItem's API operations  
@UsedBy: 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    /// <summary>
    /// AgreementLineItem related operations.
    /// </summary>
    /// <seealso cref="Apttus.SNowPS.API.Controllers.BaseController" />
    [ServiceRequestActionFilter]
    [RoutePrefix("api/v1/agreementlineitem")]
    public class AgreementLineItemController : BaseController
    {
        private readonly Repository.CLM.AgreementLineItemRepository _agreementLineItemRepo;

        public AgreementLineItemController()
        {
            _agreementLineItemRepo = Repository.CLM.AgreementLineItemRepository.Instance(AccessToken);
        }

        /// <summary>
        /// Converts the current ALI data into HTML table row format.
        /// And saves the HTML into the related Agreement header.
        /// </summary>
        /// <param name="formBody">The data.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ConvertToHtmlTableRow")]
        public IHttpActionResult ConvertToHtmlTableRow([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid id;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out id))
                {
                    return Ok(_agreementLineItemRepo.ConvertToHtmlTableRow(id));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Populates the instance details for the agreement line item.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PopulateInstanceDetails")]
        public IHttpActionResult PopulateInstanceDetails([FromBody]Dictionary<string, object> body)
        {
            try
            {
                var comparer = StringComparer.OrdinalIgnoreCase;
                body = new Dictionary<string, object>(body, comparer);

                Guid id;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out id))
                {
                    return Ok(_agreementLineItemRepo.PopulateInstanceDetails(id));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
